package dk.mada.ls.gui.dir;

import dk.mada.ls.api.dir.ObservableFileInfo;

/**
 * File size.
 * 
 * For file, this is the file's size.
 * For a folder, it is the folder's disk usage (if computed). Otherwise renders as <DIR>.
 */
public class FileSize {
	private ObservableFileInfo ofi;

	public FileSize(ObservableFileInfo ofi) {
		this.ofi = ofi;
	}

	public String getSizeStr() {
		return ofi.isDirectory() ? "<DIR>" : Long.toString(ofi.size());
	}
	
	public long getSize() {
		return ofi.size();
	}
	
	public static int comparitor(FileSize a, FileSize b) {
		int byType = TypeSorter.compareByType(a.ofi.isDirectory(), b.ofi.isDirectory());
		if (byType != 0) {
			return byType;
		}
		return Long.compare(a.getSize(), b.getSize());
	}
}
