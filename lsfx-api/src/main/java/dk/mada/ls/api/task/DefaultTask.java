package dk.mada.ls.api.task;

import java.util.Optional;

import dk.mada.ls.api.input.KeyBinding;

/**
 * Default task implementation.
 */
public abstract class DefaultTask implements Task {
    private final String name;
    private final String provider;
    private Optional<KeyBinding> defaultKeyBinding = Optional.empty();
    
    protected DefaultTask(String name, String provider) {
        this.name = name;
        this.provider = provider;
    }

    protected DefaultTask withDefaultKeyBinding(KeyBinding defaultKeyBinding) {
        this.defaultKeyBinding = Optional.of(defaultKeyBinding);
        return this;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getProvider() {
        return provider;
    }

    @Override
    public Optional<KeyBinding> getDefaultKeyBinding() {
        return defaultKeyBinding;
    }
}
