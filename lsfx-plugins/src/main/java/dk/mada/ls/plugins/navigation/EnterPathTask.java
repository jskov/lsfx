package dk.mada.ls.plugins.navigation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.mada.ls.api.dir.Dir;
import dk.mada.ls.api.dircache.model.FileInfo;
import dk.mada.ls.api.input.KeyBinding;
import dk.mada.ls.api.selection.SingleSelection;
import dk.mada.ls.api.task.Context;
import dk.mada.ls.api.task.DefaultSingleSelectionTask;
import dk.mada.ls.api.task.TaskFailedException;
import dk.mada.ls.impl.dir.Dirs;
import dk.mada.ls.plugins.LsfxDefaultPlugins;
import javafx.scene.input.KeyCode;

/**
 * Task to dive into a directory.
 */
public class EnterPathTask extends DefaultSingleSelectionTask {
    private static final Logger logger = LoggerFactory.getLogger(EnterPathTask.class);
    
    public EnterPathTask() {
        super(EnterPathTask.class.getSimpleName(), LsfxDefaultPlugins.DEFAULT_PLUGINS_PROVIDER);
        withDefaultKeyBinding(KeyBinding.create(KeyCode.ENTER));
    }

    @Override
    public boolean alwaysWantsCursorSelection() {
        return true;
    }

    @Override
    public void accept(Context context, SingleSelection selection) throws TaskFailedException {
        logger.info("Enter path with {}", selection);
        
        FileInfo selectedPath = selection.getSelection();
        if (selectedPath.isDirectory()) {
            Dir subDir = Dirs.resolve(context.activeSourceBrowser.getDir(),  selectedPath);
            context.activeSourceBrowser.setDir(subDir);
        }
    }
}
