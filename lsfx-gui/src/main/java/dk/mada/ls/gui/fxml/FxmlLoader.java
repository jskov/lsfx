package dk.mada.ls.gui.fxml;


import java.net.URL;
import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.mada.ls.cdi.CdiHelper;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;

@Singleton
public class FxmlLoader {
	private static final Logger logger = LoggerFactory.getLogger(FxmlLoader.class);

	public static FxmlLoader getInstance() {
		return CdiHelper.getInstance().getBean(FxmlLoader.class);
	}

	public Parent loadAsRoot(Initializable controller) {
		CdiHelper.getInstance().injectFields(controller);
		String fxmlPath = getFXMLName(controller.getClass());
		String cssPath = getCssName(controller.getClass());
		return load(controller, controller, fxmlPath, cssPath);
	}

	public Parent load(Initializable controller) {
		String fxmlPath = getFXMLName(controller.getClass());
		String cssPath = getCssName(controller.getClass());
		return load(controller, null, fxmlPath, cssPath);
	}

	public Parent load(Initializable controller, String fxmlPath) {
		return load(controller, null, fxmlPath, null);
	}

	public Parent load(Initializable controller, Object root, String fxmlPath, String cssPath) {
		Parent res;
		long start = System.currentTimeMillis();
		FXMLLoader loader = new FXMLLoader();
		if (root == null) {
			loader.setControllerFactory(clazz -> controller);
		} else {
			loader.setRoot(root);
			loader.setController(controller);
		}
		logger.debug("init {}:{} with loader {} and fxml {}", controller.getClass(), controller.hashCode(), loader, fxmlPath);
		
		try {
			res = loader.load(controller.getClass().getResourceAsStream(fxmlPath));
		} catch (Exception t) {
			throw new IllegalStateException("Cannot load " + fxmlPath + " into instance " + controller.getClass() + ":" + controller.hashCode(), t);
		}
		
		if (cssPath != null) {
			addCSSIfAvailable(controller, res, cssPath);
		}
		
		long time = System.currentTimeMillis() - start;
		logger.debug("Loaded FXML from {} in {}ms", fxmlPath, time);
		return res;
	}
	
	private void addCSSIfAvailable(Initializable controller, Parent parent, String cssPath) {
        URL uri = controller.getClass().getResource(cssPath);
        if (uri == null) {
            return;
        }
		logger.debug("Adding CSS from {}", cssPath);
        String uriToCss = uri.toExternalForm();
        parent.getStylesheets().add(uriToCss);
    }

	final String getFXMLName(Class<?> obj) {
		return getConventionName(obj, ".fxml");
	}

	final String getCssName(Class<?> obj) {
		return getConventionName(obj, ".css");
	}
	
	final String getConventionName(Class<?> obj, String ext) {
		String clazz = obj.getSimpleName().replaceFirst("Controller$", "");
		return clazz + ext;
	}
}
