package dk.mada.ls.gui.bookmark;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.mada.ls.api.bookmark.BookmarkStore;
import dk.mada.ls.api.bookmark.BookmarkStoreException;
import dk.mada.ls.api.dir.Dir;
import dk.mada.ls.impl.thread.ThreadHelper;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextInputDialog;

/**
 * Modal dialog for creating a new bookmark.
 */
@Singleton
public class CreateBookmark {
	private static final Logger logger = LoggerFactory.getLogger(CreateBookmark.class);
	@Inject	private BookmarkStore bookmarkStore;
	@Inject private ThreadHelper threadHelper;
	
    public void createBookmark(Dir currentDir) {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Add bookmark");
        dialog.getDialogPane().setHeaderText(null);
        dialog.getDialogPane().setGraphic(null);
        var textIsEmpty = dialog.getEditor().textProperty().isEmpty();
        dialog.getDialogPane().lookupButton(ButtonType.OK).disableProperty().bind(textIsEmpty);
        dialog.setContentText("Add bookmark with name:");

        threadHelper.fxOptSupply(dialog::showAndWait, name -> {
        	storeNewBookmark(name, currentDir);
        });
        threadHelper.fxRun(() -> dialog.getEditor().requestFocus());
    }
    
    private void storeNewBookmark(String name, Dir dir) {
    	logger.info("Store new bookmark {} for {}", name, dir);
    	try {
			bookmarkStore.addBookmark(name, dir);
		} catch (BookmarkStoreException e) {
			threadHelper.fxRun(this::showError);
		}
    }
    
    private void showError() {
		new Alert(AlertType.WARNING, "Failed to save bookmark", ButtonType.OK).showAndWait();
    }
}
