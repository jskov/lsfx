package dk.mada.ls.cdi;

import java.lang.annotation.Annotation;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.AnnotatedType;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;
import javax.enterprise.inject.spi.InjectionTarget;

import org.jboss.weld.environment.se.Weld;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CdiHelper {
	private static final Logger logger = LoggerFactory.getLogger(CdiHelper.class);
	private static final CdiHelper instance = new CdiHelper();
	
	private CdiHelper() {
	    new Weld().initialize();
	}
	
	public static CdiHelper getInstance() {
		return instance;
	}

	public <T> T getInstance(Class<T> type) {
        return getBean(type);
    }
	
	public <T> T getBean(Class<T> type) {
	    return CDI.current().select(type).get();
    }

	public void fireEvent(Object event, Annotation... qualifiers) {
	    getBeanManager().fireEvent(event, qualifiers);
	}
	
	@SuppressWarnings("unchecked")
	public <T extends Object> T injectFields(T obj) {
		logger.debug("Injecting fields into {}:{}", obj.getClass(), obj.hashCode());
		
		BeanManager beanManager = getBeanManager();
		AnnotatedType<T> type = (AnnotatedType<T>) beanManager.createAnnotatedType(obj.getClass());
		InjectionTarget<T> it = beanManager.createInjectionTarget(type);
		CreationalContext<T> ctx = beanManager.createCreationalContext(null);
		it.inject(obj, ctx);
		it.postConstruct(obj);
		
		return obj;
	}
	
	private BeanManager getBeanManager() {
		return CDI.current().getBeanManager();
	}
}
