package dk.mada.ls.gui.validation;

import javafx.beans.binding.Bindings;
import javafx.beans.value.ObservableBooleanValue;
import javafx.scene.Node;

public class Validation {
	// for validation labels
	public static final String REQUIRED_MISSING = "requiredMissing";
	public static final String REQUIRED_PROVIDED = "requiredProvided";
	
	private Validation() {
		// use static methods
	}
	
	public static void bindIdValidationHint(Node node, ObservableBooleanValue isValid) {
		node.idProperty().bind(Bindings.when(isValid).then(REQUIRED_PROVIDED).otherwise(REQUIRED_MISSING));
	}

	public static void unbindId(Node... nodes) {
		for (Node node : nodes) {
			node.idProperty().unbind();
		}
	}
}
