package dk.mada.ls.impl.external;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;

import dk.mada.ls.api.external.ExternalExecutionFailedException;
import dk.mada.ls.api.external.ExternalExecutor;


public class DefaultExecutor implements ExternalExecutor {

	@Override
	public Process launchSimple(Path dir, String... args) {
		try {
			return new ProcessBuilder(args)
				.directory(dir.toFile())
				.redirectErrorStream(true)
				.start();
			
		} catch (IOException e) {
			throw new ExternalExecutionFailedException("Failed to start process " + Arrays.toString(args), e);
		}
	}

}
