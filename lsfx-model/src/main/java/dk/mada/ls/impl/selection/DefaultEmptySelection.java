package dk.mada.ls.impl.selection;

import dk.mada.ls.api.selection.Selection;

public class DefaultEmptySelection implements Selection {

	@Override
	public boolean isSingleSelection() {
		return false;
	}

	@Override
	public boolean isMultiSelection() {
		return false;
	}
}
