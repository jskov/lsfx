package dk.mada.ls.api.selection;

public interface Selection {
	boolean isSingleSelection();
	boolean isMultiSelection();
}
