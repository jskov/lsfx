package dk.mada.ls.gui.dir;

/**
 * Sorting for file elements based on type and user preference.
 * 
 * User may select to sort files by type (dir/file) which is the default.
 * But this can also be disabled (hardwired for now) so they appear mixed.
 */
public class TypeSorter {
	private static final boolean PREFERENCE_SORT_SEPARATES_BY_TYPE = true;

	public static int compareByType(boolean aIsDir, boolean bIsDir) {
		if (!PREFERENCE_SORT_SEPARATES_BY_TYPE) {
			return 0;
		}
		if (aIsDir && !bIsDir) {
			return -1;
		}
		if (!aIsDir && bIsDir) {
			return 1;
		}
		return 0;
	}

}
