package dk.mada.ls.impl.dircache;

import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import dk.mada.ls.api.dir.Dir;
import dk.mada.ls.api.dircache.model.DirAttrs;
import dk.mada.ls.api.dircache.model.DirDelta;
import dk.mada.ls.api.dircache.model.DirInfo;
import dk.mada.ls.api.dircache.model.FileInfo;
import dk.mada.ls.api.dircache.model.ImmutableDirAttrs;
import dk.mada.ls.api.dircache.model.ImmutableDirDelta;
import dk.mada.ls.api.dircache.model.ImmutableDirInfo;

/**
 * Operations to update DirCache based on different sources.
 */
public class HostDirCacheUpdater {
    private static final int DELTA_COUNTER_BASE = 0;
    private static final DirDelta EMPTY_DIR_DELTA = ImmutableDirDelta.builder()
            .added(Collections.emptySet())
            .removedNames(Collections.emptySet())
            .changed(Collections.emptySet())
            .build();
    
    /**
     * Updating with directory delta from remote host.
     * @param dir
     * @param dirDelta
     */
    public void updateWithRemoteDirDelta(HostDirCache hostDirCache, RemoteDirDelta remoteDirDelta) {
        DirAttrs dirAttrs = remoteDirDelta.dirAttrs();
        DirDelta dirDelta = remoteDirDelta.dirDelta();
        hostDirCache.createOrUpdate(dirAttrs.dir(),
                () -> createBaseFromDelta(dirAttrs.dir(), dirAttrs, dirDelta.added(), dirDelta.changed()),
                oldInfo -> createNextFromDelta(oldInfo, dirAttrs, dirDelta));
    }
        
    /**
     * Updating with file delta from local host.
     * @param dir
     * @param dirAttrs
     * @param added
     * @param changed
     * @param removedNames
     */
    public void updateWithFileDelta(HostDirCache hostDirCache, Dir dir, BasicFileAttributes dirFileAttrs, Set<FileInfo> added, Set<FileInfo> changed, Set<String> removedNames) {
        DirAttrs dirAttrs = toDirAttrs(dir, dirFileAttrs);
        hostDirCache.createOrUpdate(dir,
                () -> createBaseFromDelta(dir, dirAttrs, added, changed), 
                oldInfo -> createNextFromDelta(oldInfo, dirAttrs, makeDirDelta(added, changed, removedNames)));
    }

    /**
     * Updating with file list from local host.
     * @param dir
     * @param dirAttrs
     * @param children
     */
    public void updateWithList(HostDirCache hostDirCache, Dir dir, BasicFileAttributes dirFileAttrs, Set<FileInfo> children) {
        DirAttrs dirAttrs = toDirAttrs(dir, dirFileAttrs);
        hostDirCache.createOrUpdate(dir,
                () -> createBaseFromList(dir, dirAttrs, children),
                oldInfo -> createNext(oldInfo, dirAttrs, children, computeDelta(oldInfo, children)));
    }

    private DirInfo createNextFromDelta(DirInfo oldInfo, DirAttrs dirAttrs, DirDelta dirDelta) {
        return createNext(oldInfo, dirAttrs, computeUpdatedChildren(oldInfo, dirDelta), dirDelta);
    }

    private DirInfo createNext(DirInfo oldInfo, DirAttrs dirAttrs, Set<FileInfo> newChildren, DirDelta delta) {
        return ImmutableDirInfo.builder()
                .dir(oldInfo.dir())
                .deltaCounter(oldInfo.deltaCounter() + 1)
                .dirAttrs(dirAttrs)
                .children(newChildren)
                .dirDelta(delta)
                .build();
    }
    
    private DirInfo createBaseFromDelta(Dir dir, DirAttrs dirAttrs, Set<FileInfo> added, Set<FileInfo> changed) {
        Set<FileInfo> children = new HashSet<>(added);
        children.addAll(changed);
        return createBaseFromList(dir, dirAttrs, children);
    }

    private DirInfo createBaseFromList(Dir dir, DirAttrs dirAttrs, Set<FileInfo> children) {
        return ImmutableDirInfo.builder()
                .dir(dir)
                .deltaCounter(DELTA_COUNTER_BASE)
                .dirAttrs(dirAttrs)
                .children(children)
                .dirDelta(EMPTY_DIR_DELTA)
                .build();
    }

    private DirDelta computeDelta(DirInfo oldInfo, Set<FileInfo> children) {
        Map<Integer, FileInfo> idToOldChildren = oldInfo.children().stream().collect(Collectors.toMap(FileInfo::dirId, Function.identity()));
        
        Set<FileInfo> added = new HashSet<>();
        Set<FileInfo> changed = new HashSet<>();
        
        for (FileInfo c : children) {
            FileInfo oldChild = idToOldChildren.get(c.dirId());
            if (oldChild == null) {
                added.add(c);
            } else if (!oldChild.equals(c)) {
                changed.add(c);
            }
            idToOldChildren.remove(c.dirId());
        }
        Set<String> removedNames = idToOldChildren.values().stream().map(fi -> fi.name()).collect(Collectors.toSet());

        return makeDirDelta(added, changed, removedNames);
    }

    private DirDelta makeDirDelta(Set<FileInfo> added, Set<FileInfo> changed, Set<String> removedNames) {
        return ImmutableDirDelta.builder()
                .added(added)
                .changed(changed)
                .removedNames(removedNames)
                .build();
    }

    private Set<FileInfo> computeUpdatedChildren(DirInfo oldInfo, DirDelta dirDelta) {
        Set<FileInfo> newChildren = new HashSet<>(oldInfo.children());
        newChildren.removeIf(c -> dirDelta.removedNames().contains(c.name()) || dirDelta.changed().stream().anyMatch(fi -> fi.dirId() == c.dirId()));
        newChildren.addAll(dirDelta.added());
        newChildren.addAll(dirDelta.changed());
        return newChildren;
    }
    
    private DirAttrs toDirAttrs(Dir dir, BasicFileAttributes attrs) {
        return ImmutableDirAttrs.builder()
              .creationTime(attrs.creationTime().toMillis())
              .lastModifiedTime(attrs.lastAccessTime().toMillis())
              .dir(dir)
              .build();
    }
}
