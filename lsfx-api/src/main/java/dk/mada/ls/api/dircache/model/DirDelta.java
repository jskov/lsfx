package dk.mada.ls.api.dircache.model;

import java.util.Set;

import org.immutables.value.Value.Immutable;

/**
 * Describes changes between two states of a directory.
 */
@Immutable
public interface DirDelta {
    Set<FileInfo> added();
    Set<FileInfo> changed();
    /**
     * Set of removed names.
     * 
     * TODO: For zip archives this will have to become richer.
     */
    Set<String> removedNames();
}
