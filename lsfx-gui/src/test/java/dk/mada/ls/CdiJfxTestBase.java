package dk.mada.ls;

import java.io.InputStream;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.logging.LogManager;

import javax.swing.SwingUtilities;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import dk.mada.ls.cdi.CdiHelper;
import javafx.embed.swing.JFXPanel;

// From https://stackoverflow.com/questions/28501307/javafx-toolkit-not-initialized-in-one-test-class-but-not-two-others-where-is
//@RunWith(CdiRunner.class) - Not Junit5 compatible yet.

@TestInstance(Lifecycle.PER_CLASS)
public abstract class CdiJfxTestBase {

    @BeforeAll
    public void staticTestSetup() throws ExceptionInInitializerError, InterruptedException {
        loadLoggerConfiguration();
        setupJavaFX();
        CdiHelper.getInstance().injectFields(this);
    }
    
    private static void setupJavaFX() throws ExceptionInInitializerError, InterruptedException {
        final CountDownLatch latch = new CountDownLatch(1);
        SwingUtilities.invokeLater(() -> {
            new JFXPanel(); // initializes JavaFX environment
            latch.countDown();
        });

        if (!latch.await(5L, TimeUnit.SECONDS)) {
            throw new ExceptionInInitializerError();
        }
    }
    
    private static void loadLoggerConfiguration() {
        final LogManager logManager = LogManager.getLogManager();
        try (final InputStream is = LsFxMain.class.getResourceAsStream("/logging-test.properties")) {
            logManager.readConfiguration(is);
        } catch (Exception e) {
            throw new IllegalStateException("Failed to load logger configuration", e);
        }
    }

}
