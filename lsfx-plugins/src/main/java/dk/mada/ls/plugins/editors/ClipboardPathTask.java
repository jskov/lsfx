package dk.mada.ls.plugins.editors;

import java.io.File;
import java.util.stream.Collectors;

import dk.mada.ls.api.dir.Dir;
import dk.mada.ls.api.input.KeyBinding;
import dk.mada.ls.api.selection.MultiSelection;
import dk.mada.ls.api.task.Context;
import dk.mada.ls.api.task.DefaultMultiSelectionTask;
import dk.mada.ls.api.task.TaskFailedException;
import dk.mada.ls.plugins.LsfxDefaultPlugins;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.KeyCode;

/**
 * Task to copy the path of the selected file(s) to the clipboard.
 */
public class ClipboardPathTask extends DefaultMultiSelectionTask {
    public ClipboardPathTask() {
        super(ClipboardPathTask.class.getSimpleName(), LsfxDefaultPlugins.DEFAULT_PLUGINS_PROVIDER);
        withDefaultKeyBinding(KeyBinding.createWithAlt(KeyCode.ENTER));
    }

    @Override
    public void accept(Context context, MultiSelection selection) throws TaskFailedException {
        Dir dir = context.activeSourceBrowser.getDir();
        
        String paths = selection.getSelection().stream()
        	.map(f -> dir.path() + f.name())
        	.collect(Collectors.joining(File.pathSeparator));

        ClipboardContent content = new ClipboardContent();
        content.putString(paths);
        Clipboard.getSystemClipboard().setContent(content);
    }
}
