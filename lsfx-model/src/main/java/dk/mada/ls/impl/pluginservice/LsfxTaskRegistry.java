package dk.mada.ls.impl.pluginservice;

import javax.enterprise.inject.spi.CDI;
import javax.inject.Inject;
import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.mada.ls.api.task.Task;
import dk.mada.ls.api.task.TaskRegistry;
import dk.mada.ls.impl.input.DefaultKeyInputManager;


/**
 * for now only registers tasks by their keyboard binding.
 */
@Singleton
public class LsfxTaskRegistry implements TaskRegistry {
    private static final Logger logger = LoggerFactory.getLogger(LsfxTaskRegistry.class);

    @Inject private DefaultKeyInputManager keyInputManager;
    
    @Override
    public <T extends Task > void addTask(Class<T> taskType) {
        logger.debug("Add task {}", taskType);
        
        Task task = CDI.current().select(taskType).get();
        if (task != null) {
        	task.getDefaultKeyBinding().ifPresent( kb -> keyInputManager.addDefaultBinding(kb, task) );
        } else {
        	logger.warn("Failed to get task {} via CDI", taskType);
        }
    }
}
