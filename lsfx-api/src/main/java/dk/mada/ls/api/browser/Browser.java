package dk.mada.ls.api.browser;


import java.util.List;
import java.util.Optional;

import dk.mada.ls.api.dir.Dir;
import dk.mada.ls.api.dir.ObservableDir;
import dk.mada.ls.api.dircache.model.FileInfo;
import dk.mada.ls.api.selection.Selection;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyListProperty;
import javafx.beans.property.ReadOnlyObjectProperty;

public interface Browser {
    Dir getDir();
    /**
     * Set new dir in browser.
     * 
     * @param dir
     */
    void setDir(Dir dir);
    /**
     * Set new dir in browser, and set cursor at sub-dir.
     * 
     * Used when navigating up from a sub-dir to a parent dir.
     * If the sub-dir does match a name, cusor is left at the top.
     * 
     * @param dir
     * @param selectCursor
     */
    void setDir(Dir dir, Dir selectCursor);
    ReadOnlyObjectProperty<ObservableDir> obsDirProperty();

    ReadOnlyIntegerProperty cursorIndexProperty();
    Optional<Integer> getCursorIndex();
    Optional<FileInfo> getCursorItem();
    void setCursorIndex(int cursorIndex);

    ReadOnlyListProperty<Integer> selectedIndicesProperty();
    Selection getSelection(boolean preferCursorSelection);
    void selectIndices(List<Integer> selectedIndices);
    
    /**
     * Opens bookmarks menu relative to browser.
     */
    void showBookmarks();
}