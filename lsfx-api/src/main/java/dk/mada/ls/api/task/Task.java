package dk.mada.ls.api.task;

import java.util.Optional;

import dk.mada.ls.api.input.KeyBinding;

public interface Task {
    Optional<KeyBinding> getDefaultKeyBinding();
    String getName();
    String getProvider();
}
