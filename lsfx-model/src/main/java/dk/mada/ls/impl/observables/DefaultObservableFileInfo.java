package dk.mada.ls.impl.observables;

import dk.mada.ls.api.dir.ObservableFileInfo;
import dk.mada.ls.api.dircache.model.FileInfo;
import javafx.beans.property.ReadOnlyLongProperty;
import javafx.beans.property.ReadOnlyLongWrapper;

/**
 * Observable file.
 * 
 * Only the properties of the file that can actually change are
 * observable.
 */
public class DefaultObservableFileInfo implements ObservableFileInfo {
    private FileInfo fi;
    
    private ReadOnlyLongWrapper sizeProperty = new ReadOnlyLongWrapper();
    private ReadOnlyLongWrapper lastModifiedTimeProperty = new ReadOnlyLongWrapper();
    private ReadOnlyLongWrapper creationTimeProperty = new ReadOnlyLongWrapper();

    DefaultObservableFileInfo(FileInfo fi) {
        update(fi);
    }
    
    @Override
    public void update(FileInfo fi) {
        this.fi = fi;
        sizeProperty.set(fi.size());
        lastModifiedTimeProperty.set(fi.lastModifiedTime());
        creationTimeProperty.set(fi.creationTime());
    }
    
    public FileInfo getFileInfo() {
        return fi;
    }
    
    public String name() {
        return fi.name();
    }
    
    public String getMainName() {
        String n = name();
        int li = n.lastIndexOf(".");
        return (li == -1 || li == 0) ? n : n.substring(0, li-1);
    }
    
    public String getExt() {
        String n = name();
        int li = n.lastIndexOf(".");
        return (li == -1 || li == 0) ? "" : n.substring(li);
    }
    
    public int key() {
        return fi.key();
    }

    @Override
    public long size() {
        return fi.size();
    }
    
    public int dirId() {
        return fi.dirId();
    }
    
    public boolean isDirectory() {
        return fi.isDirectory();
    }
    
    public boolean isSymbolicLink() {
        return fi.isSymbolicLink();
    }
    
    public ReadOnlyLongProperty sizeProperty() {
        return sizeProperty.getReadOnlyProperty();
    }

    public ReadOnlyLongProperty lastModifiedTimeProperty() {
        return lastModifiedTimeProperty.getReadOnlyProperty();
    }

    public ReadOnlyLongProperty creationTimeProperty() {
        return creationTimeProperty.getReadOnlyProperty();
    }

    @Override
    public String toString() {
        return "ObservableFileInfo [name()=" + name() + "]";
    }
}
