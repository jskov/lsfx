package dk.mada.ls.impl.dircache;

import java.nio.file.Path;
import java.nio.file.WatchKey;
import java.util.HashMap;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * Bi-directional map between a NIO Path and a WatchKey.
 * 
 * Watches are created for Paths, returning a Key.
 * But this package uses a single event handler for all Keys,
 * so it needs a way to get from Key to Path again.  
 */
public class LocalPathToWatchKeyMap {
    private HashMap<Path, WatchKey> path2key = new HashMap<>();
    private HashMap<WatchKey, Path> key2path = new HashMap<>();
    
    public synchronized void clear() {
        path2key = new HashMap<>();
        key2path = new HashMap<>();
    }
    
    public synchronized boolean isEmpty() {
        return path2key.isEmpty();
    }
    
    public synchronized void addIfPathNotContained(Path path, Function<Path, WatchKey> keySupplier) {
        if (!path2key.containsKey(path)) {
            WatchKey watchKey = keySupplier.apply(path);
            path2key.put(path, watchKey);
            key2path.put(watchKey, path);
        }
    }
    
    public synchronized void removeIfPathContained(Path path, BiConsumer<Path, WatchKey> thenDo) {
        if (path2key.containsKey(path)) {
            WatchKey key = path2key.get(path);
            thenDo.accept(path, key);
            path2key.remove(path);
            key2path.remove(key);
        }
    }
    
    /**
     * Returns path for a given key.
     * Path may have been unregistered, hence the Optional return value.
     */
    public synchronized Optional<Path> getPath(WatchKey key) {
        return Optional.ofNullable(key2path.get(key));
    }
}
