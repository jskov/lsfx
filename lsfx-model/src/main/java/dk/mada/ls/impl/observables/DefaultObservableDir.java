package dk.mada.ls.impl.observables;

import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ListIterator;
import java.util.concurrent.atomic.AtomicBoolean;

import dk.mada.ls.api.dir.Dir;
import dk.mada.ls.api.dir.ObservableDir;
import dk.mada.ls.api.dir.ObservableFileInfo;
import dk.mada.ls.api.dircache.model.DirAttrs;
import dk.mada.ls.api.dircache.model.DirDelta;
import dk.mada.ls.api.dircache.model.DirInfo;
import javafx.application.Platform;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.beans.property.ReadOnlyListProperty;
import javafx.beans.property.ReadOnlyListWrapper;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;

/**
 * Observable directory.
 * 
 * Contains the active state as seen by the client, and the latest state reported by the backend.
 * The active state can be refreshed by explicit call or automatically.
 */
public class DefaultObservableDir implements ObservableDir {
    private static final Logger logger = LoggerFactory.getLogger(DefaultObservableDir.class);
    private Dir dir;
    private DirInfo activeDirInfo;
    private DirInfo latestDirInfo;
    
    private ReadOnlyStringWrapper summary = new ReadOnlyStringWrapper();
    private ReadOnlyIntegerWrapper activeCounterProperty = new ReadOnlyIntegerWrapper(-1);
    private ReadOnlyIntegerWrapper latestCounterProperty = new ReadOnlyIntegerWrapper(-1);
    private BooleanBinding isGuiRefreshedProperty = activeCounterProperty.isEqualTo(latestCounterProperty);
    private ReadOnlyListWrapper<ObservableFileInfo> childrenProperty = new ReadOnlyListWrapper<>(FXCollections.observableArrayList());
    private SimpleBooleanProperty autoRefreshProperty = new SimpleBooleanProperty();
    private ReadOnlyObjectWrapper<DirAttrs> dirAttrsProperty = new ReadOnlyObjectWrapper<>();
    private AtomicBoolean acceptNextUpdate = new AtomicBoolean();
    
    public DefaultObservableDir(Dir dir) {
        this.dir = dir;
        
        autoRefreshProperty.addListener((obj, oldIsSelected, newIsSelected) -> refresh());
    }
    
    /**
     * Call when this dir has just been revealed in a browser,
     * to ensure the state gets updated by the refresh event
     * triggered by the DirCache.
     */
    public void justRevealedAcceptNextUpdate() {
    	acceptNextUpdate.set(true);
    }
    
    public Dir getDir() {
        return dir;
    }
    
    public ReadOnlyIntegerProperty activeCounterProperty() {
        return activeCounterProperty.getReadOnlyProperty();
    }

    public ReadOnlyIntegerProperty latestCounterProperty() {
        return latestCounterProperty.getReadOnlyProperty();
    }

    @Override
    public ReadOnlyListProperty<ObservableFileInfo> childrenProperty() {
        return childrenProperty.getReadOnlyProperty();
    }
    
    @Override
    public BooleanBinding isGuiRefreshedProperty() {
        return isGuiRefreshedProperty;
    }
    
    @Override
    public boolean isGuiRefreshed() {
        return isGuiRefreshedProperty.get();
    }
    
    @Override
    public BooleanProperty autoRefreshProperty() {
        return autoRefreshProperty;
    }
    
    void updateDirInfo(DirInfo dirInfo) {
        Platform.runLater(() -> _updateDirInfo(dirInfo));
    }
    
    private void _updateDirInfo(DirInfo dirInfo) {
        this.latestDirInfo = dirInfo;
        
        latestCounterProperty.set(dirInfo.deltaCounter());
        
        if (acceptNextUpdate.getAndSet(false) || dirInfo.deltaCounter() == 0 || autoRefreshProperty.get()) {
            refresh();
        }
    }

    /**
     * Refresh active information with latest information.
     * 
     * Does not actually call out for refreshed data, only updates internal state.
     */
    public void refresh() {
        if (activeDirInfo == latestDirInfo) {
            return;
        }
        
        boolean useDelta = activeDirInfo != null && ((activeDirInfo.deltaCounter()+1) == latestDirInfo.deltaCounter());
        logger.info("Refreshing {} using {}/{}", dir.path(), latestDirInfo.deltaCounter(), useDelta ? "delta" : "set");
        
        activeDirInfo = latestDirInfo;
        dirAttrsProperty.set(activeDirInfo.dirAttrs());

        if (useDelta) {
            applyDelta();
        } else {
            childrenProperty.setAll(activeDirInfo.children().stream()
                                            .map(DefaultObservableFileInfo::new)
                                            .peek(ofi -> logger.debug(" add {}", ofi.name()))
                                            .collect(Collectors.toList()));
        }
        
        activeCounterProperty.set(activeDirInfo.deltaCounter());
        
        long dirCount = childrenProperty.stream()
            .filter(ofi -> ofi.isDirectory())
            .count();
        summary.set(dirCount + " dirs, " + childrenProperty.size() + " files total");
    }
    
    private void applyDelta() {
        DirDelta dirDelta = activeDirInfo.dirDelta();
        for (ListIterator<ObservableFileInfo> ix = childrenProperty.listIterator(); ix.hasNext();) {
            ObservableFileInfo ofi = ix.next();
            if (dirDelta.removedNames().contains(ofi.name())) {
                ix.remove();
            } else {
                dirDelta.changed().stream()
                        .filter(fi -> fi.dirId() == ofi.dirId())
                        .findFirst()
                        .ifPresent(ofi::update); 
            }
        }
        childrenProperty.addAll(dirDelta.added().stream()
                                        .map(DefaultObservableFileInfo::new)
                                        .peek(ofi -> logger.debug(" delta {}", ofi.name()))
                                        .collect(Collectors.toList()));
    }
}
