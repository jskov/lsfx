package dk.mada.ls.api.view;

import java.util.List;
import java.util.Optional;

import dk.mada.ls.api.browser.Browser;
import dk.mada.ls.api.dir.Dir;
import dk.mada.ls.api.task.Context;

/**
 * Window in the application.
 * May contain one or more tabs. May be linked to a neighbor.
 */
public interface Window {
    List<Tab> getTabs();
    
    Browser getBrowser();
    
    /**
     * Companion window to current window.
     * Windows can be grouped together two-and-two for NortonCommander behavior.
     * @return
     */
    Optional<Window> getCompanion();
    
    void showDir(Dir dir);
    
    /**
     * Current context in the form of a primary and secondary tab.
     * @return
     */
    Context getContext();
    
    default void losingFocus() {};
    void gettingFocus();
}
