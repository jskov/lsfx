package dk.mada.ls.impl.selection;

import dk.mada.ls.api.dircache.model.FileInfo;
import dk.mada.ls.api.selection.SingleSelection;

public class DefaultSingleSelection implements SingleSelection {
    private FileInfo selection;
    
    public DefaultSingleSelection(FileInfo selection) {
        this.selection = selection;
    }

    public FileInfo getSelection() {
        return selection;
    }

    @Override
    public String toString() {
        return "DefaultSingleSelection [selection=" + selection + "]";
    }

	@Override
	public boolean isSingleSelection() {
		return true;
	}

	@Override
	public boolean isMultiSelection() {
		return false;
	}
}
