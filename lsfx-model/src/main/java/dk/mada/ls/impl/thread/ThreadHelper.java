package dk.mada.ls.impl.thread;

import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.function.Supplier;

import javax.inject.Singleton;

import javafx.application.Platform;

/**
 * Thread handling helper.
 */
@Singleton
public class ThreadHelper {
	private ExecutorService execService = Executors.newFixedThreadPool(5);
	
	public void runWorker(Runnable r) {
		execService.submit(r);
	}
	
	public void fxRun(Runnable fxRunnable) {
		Platform.runLater(fxRunnable);
	}
	
	public <T> void fxSupply(Supplier<T> fxSupplier, Consumer<T> workConsumer) {
		Platform.runLater(() -> {
			T v = fxSupplier.get();
			
			runWorker(() -> {
				workConsumer.accept(v);
			});
		});
	}

	public <T> void fxOptSupply(Supplier<Optional<T>> fxSupplier, Consumer<? super T> workConsumer) {
		Platform.runLater(() -> {
			fxSupplier.get().ifPresent(v -> {
				runWorker(() -> {
					workConsumer.accept(v);
				});
			});
		});
	}
}
