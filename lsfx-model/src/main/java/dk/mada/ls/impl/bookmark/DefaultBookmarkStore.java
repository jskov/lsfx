package dk.mada.ls.impl.bookmark;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.mada.ls.api.bookmark.Bookmark;
import dk.mada.ls.api.bookmark.BookmarkStore;
import dk.mada.ls.api.bookmark.BookmarkStoreException;
import dk.mada.ls.api.bookmark.ImmutableBookmark;
import dk.mada.ls.api.dir.Dir;
import dk.mada.ls.impl.dir.Dirs;
import dk.mada.ls.impl.prefs.Preferences;

/**
 * Default implementation of bookmark store.
 * 
 * Saves key/value pairs to file where user controls key (name of bookmark)
 * the the value is a host:path combination.
 */
@Singleton
public class DefaultBookmarkStore implements BookmarkStore {
	private static final Logger logger = LoggerFactory.getLogger(DefaultBookmarkStore.class);
	@Inject private Preferences prefs;
	
	private List<Bookmark> bookmarks = new ArrayList<>();
	
	@Override
	public void addBookmark(String name, Dir dir) throws BookmarkStoreException {
		bookmarks.add(ImmutableBookmark.builder()
				.name(name)
				.location(dir)
				.build());
		save();
	}

	@Override
	public List<Bookmark> getBookmarks() {
		return Collections.unmodifiableList(bookmarks);
	}

	public void load() throws BookmarkStoreException {
		Path file = getBookmarksFile();
		if (!Files.exists(file)) {
			return;
		}
		
		logger.info("Loading from {}", file);
		try (var reader = Files.newBufferedReader(file, StandardCharsets.UTF_8)) {
			var props = new Properties();
			props.load(reader);
			
			props.stringPropertyNames()
				.forEach(k -> addBookmarkIfValid(k, props.getProperty(k)));
		} catch (IOException e) {
			throw new BookmarkStoreException("Failed to load bookmarks from " + file, e);
		}
	}

	private void addBookmarkIfValid(String key, String value) {
		var parts = value.split(":", 2);
		if (parts.length != 2) {
			return;
		}
		
		var host = parts[0];
		var path = parts[1];
		var dir = Dirs.from(host, path);
		var bookmark = ImmutableBookmark.builder()
			.name(key)
			.location(dir)
			.build();
		
		bookmarks.add(bookmark);
	}
	
	private void save() throws BookmarkStoreException {
		Path file = getBookmarksFile();
		
		logger.info("Writer bookmarks to {}", file);
		
		var props = new Properties();
		bookmarks.forEach(bm -> props.setProperty(bm.name(), bm.location().host().hostname() + ":" + bm.location().path()));
		try {
			Files.createDirectories(file.getParent());
			try (var writer = Files.newBufferedWriter(file, StandardCharsets.UTF_8)) {
				props.store(writer, null);
			}
		} catch (IOException e) {
			throw new BookmarkStoreException("Failed to save bookmarks to " + file, e);
		}
	}
	
	private Path getBookmarksFile() {
		return prefs.getPreferencesDir().resolve("bookmarks.propertis");
	}
}

