package dk.mada.ls.impl.observables;

import java.util.HashMap;

import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Singleton;

import dk.mada.ls.api.dir.Dir;
import dk.mada.ls.api.dircache.DirCache;
import dk.mada.ls.api.dircache.model.DirInfo;
import javafx.beans.property.ReadOnlyMapProperty;
import javafx.beans.property.ReadOnlyMapWrapper;
import javafx.collections.FXCollections;

/**
 * Factory for observable directories.
 */
@Singleton
public class ObservableDirs {
    private ReadOnlyMapWrapper<Dir, DefaultObservableDir> dirsProperty = new ReadOnlyMapWrapper<>(FXCollections.observableMap(new HashMap<>()));
    
    @Inject private DirCache dirCache;
    
    public ReadOnlyMapProperty<Dir, DefaultObservableDir> dirsProperty() {
        return dirsProperty.getReadOnlyProperty();
    }
    
    public DefaultObservableDir get(Dir dir) {
        DefaultObservableDir od;
        synchronized(dirsProperty) {
            od = dirsProperty.computeIfAbsent(dir, DefaultObservableDir::new);
        }
     
        dirCache.subscribe(dir);
        return od;
    }
    
    public void pause(Dir dir) {
        dirCache.unsubscribe(dir);
    }
    
    public void resume(Dir dir) {
        dirCache.unsubscribe(dir);
    }
    
    public void onChangedDir(@Observes DirInfo dirInfo) {
        Dir dir = dirInfo.dir();
        DefaultObservableDir od;
        synchronized(dirsProperty) {
            od = dirsProperty.computeIfAbsent(dir, DefaultObservableDir::new);
        }
        od.updateDirInfo(dirInfo);
    }
}
