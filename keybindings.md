# Key Bindings #

* *ESC* Exits lsfx
* *TAB* Switch to other editor pane
* *ENTER* Dive into directory
* *BACKSPACE* Go to parent directory

* *Alt+ENTER* Copy path of selected file(s) to ClipBoard

* *F3* Views current selected file

* *Cursor/space* Standard list selection 
