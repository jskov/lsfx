package dk.mada.ls.api;

/**
 * Used to annotate fields in @immutable interfaces that can be null.
 */
public @interface Nullable {
}
