package dk.mada.ls.api.selection;

import dk.mada.ls.api.dircache.model.FileInfo;

/**
 * Represents a single-file selection in the source browser (or the file under the cursor
 * if there is no selection *or* if this is what the consuming task prefers).
 */
public interface SingleSelection extends Selection {
    FileInfo getSelection();
}
