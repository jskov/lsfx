package dk.mada.ls.api.external;

/**
 * Exception used to wrap external execution failures.
 */
public class ExternalExecutionFailedException extends RuntimeException {
	public ExternalExecutionFailedException(String message, Throwable cause) {
		super(message, cause);
	}
}
