package dk.mada.ls.gui.dir;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.mada.ls.api.browser.Browser;
import dk.mada.ls.api.dir.ObservableDir;
import dk.mada.ls.api.dir.ObservableFileInfo;
import dk.mada.ls.gui.DefaultWindow;
import dk.mada.ls.gui.bookmark.BookmarksHandler;
import dk.mada.ls.gui.fxml.FxmlControllerBase;
import dk.mada.ls.impl.browser.DefaultBrowser;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.ListChangeListener.Change;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

public class DirList extends FxmlControllerBase {
    private static final Logger logger = LoggerFactory.getLogger(DirList.class);
    
    @FXML private AnchorPane root;
    @FXML private ToggleButton refresh;
    @FXML private TextField path;
    @FXML private TableView<ObservableFileInfo> list;
    @FXML private TableColumn<ObservableFileInfo, String> colName;
    @FXML private TableColumn<ObservableFileInfo, String> colExt;
    @FXML private TableColumn<ObservableFileInfo, FileSize> colSize;
    @FXML private TableColumn<ObservableFileInfo, Long> colDate;
    @FXML private Label status;
    @FXML private Button bookmarks;

    @Inject private BookmarksHandler bookmarkHandler;
    
    private AtomicBoolean lockSettingBrowserSelection = new AtomicBoolean();
    private AtomicBoolean lockGettingBrowserSelection = new AtomicBoolean();
    private AtomicBoolean lockSettingBrowserFocus = new AtomicBoolean();
    private AtomicBoolean lockGettingBrowserFocus = new AtomicBoolean();

    private DefaultBrowser browser;
    private ObservableList<ObservableFileInfo> modelList;
    private SortedList<ObservableFileInfo> sortedList;
    
    private InvalidationListener selectedIndicesPropertyListener = (Observable c) -> browserSelectionChanged();
    private ChangeListener<? super Number> cursorIndexListener = (obj, ov, nv) -> browserCursorChanged(nv.intValue());
    private ChangeListener<? super ObservableDir> obsDirListener = (obj, ov, nv) -> onObsDirChanged(ov, nv);

	private DefaultWindow parentWindow;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        logger.info("init DirList {}", root);

        colName.setCellValueFactory(new PropertyValueFactory<>("mainName"));
        colName.setComparator(NameSorter::compareByName);
        colName.setCellFactory(tc -> new NameCell<ObservableFileInfo>());
        colExt.setCellValueFactory(new PropertyValueFactory<>("ext"));
        colSize.setCellValueFactory(ofi -> new SimpleObjectProperty<>(new FileSize(ofi.getValue())));
        colSize.setCellFactory(tc -> new SizeCell<ObservableFileInfo>());
        colSize.setComparator(FileSize::comparitor);
        colDate.setCellValueFactory(new PropertyValueFactory<>("lastModifiedTime"));
        colDate.setCellFactory(tc -> new DateCell<ObservableFileInfo>());

        list.focusedProperty().addListener((obj, ov, nv) -> parentWindow.focusFromList(nv));
        list.getFocusModel().focusedIndexProperty().addListener((obj, ov, focusIndex) -> listFocusChanged(focusIndex.intValue())); 

        list.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        list.getSelectionModel().getSelectedIndices().addListener((Change<? extends Integer> c) -> listSelectionChanged());
        
        bookmarks.setOnAction(ae -> bookmarkHandler.showForAnchor(bookmarks));
    }
    
    public void setBrowser(DefaultBrowser newBrowser) {
        if (browser != null) {
        	logger.debug("Release from {}", browser);
            browser.selectedIndicesProperty().removeListener(selectedIndicesPropertyListener);
            browser.cursorIndexProperty().removeListener(cursorIndexListener);
            browser.obsDirProperty().removeListener(obsDirListener);
            browser.setOnShowBookmarks(null);
        }
        
        browser = newBrowser;
    	logger.debug("Attach to {}", browser);

        browser.selectedIndicesProperty().addListener(selectedIndicesPropertyListener);
        browser.cursorIndexProperty().addListener(cursorIndexListener);
        browser.obsDirProperty().addListener(obsDirListener);
        browser.setOnShowBookmarks(this::showBookmarks);
    }
    
    private void showBookmarks() {
    	bookmarkHandler.showForAnchor(bookmarks);
    	
    }

	public void setWidow(DefaultWindow defaultWindow) {
		this.parentWindow = defaultWindow;
	}

    public Browser getBrowser() {
        return browser;
    }
    
    public void requestFocus() {
    	list.requestFocus();
    }
    
    private void browserSelectionChanged() {
        ignoreWhileUpdating(lockSettingBrowserSelection, lockGettingBrowserSelection, () -> {
        	TableViewSelectionModel<ObservableFileInfo> sm = list.getSelectionModel();
        	sm.clearSelection();
            browser.selectedIndicesProperty().stream()
            			.map(modelList::get)
        				.forEach(sm::select);
        				
            logger.info("Model changed selection {}", sm);
        });
    }

    private void listSelectionChanged() {
        ignoreWhileUpdating(lockGettingBrowserSelection, lockSettingBrowserSelection, () -> {
            List<Integer> selectedIndices = list.getSelectionModel().getSelectedIndices().stream()
            		.map(this::mapViewIndexToModelIndex)
            		.collect(Collectors.toList());
			logger.info("Change {}", selectedIndices);
            browser.selectIndices(selectedIndices);
        });
    }

	private int mapViewIndexToModelIndex(int viewIndex) {
		if (viewIndex == -1) {
			return -1;
		}
		return sortedList.getSourceIndexFor(modelList, viewIndex);
	}
    
	/**
	 * Used to lock model/view for updates, while view/model is updating in
	 * the other direction.
	 */
    private void ignoreWhileUpdating(AtomicBoolean lock, AtomicBoolean wrapLock, Runnable r) {
        if (!lock.get()) {
            wrapLock.set(true);
            r.run();
            wrapLock.set(false);
        }        
    }

    private void listFocusChanged(int viewFocusIndex) {
        ignoreWhileUpdating(lockGettingBrowserFocus, lockSettingBrowserFocus, () -> {
            int modelViewIndex = mapViewIndexToModelIndex(viewFocusIndex);
			browser.setCursorIndex(modelViewIndex);
        });
    }
    
    private void browserCursorChanged(int modelIndex) {
        ignoreWhileUpdating(lockSettingBrowserFocus, lockGettingBrowserFocus, () -> {
        	ObservableFileInfo ofi = modelList.get(modelIndex);
        	int viewIndex = list.getItems().indexOf(ofi);
        	list.getFocusModel().focus(viewIndex);
        });
    }

    private void onObsDirChanged(ObservableDir oldObsDir, ObservableDir newObsDir) {
        if (oldObsDir != null) {
            refresh.selectedProperty().unbindBidirectional(oldObsDir.autoRefreshProperty());
            refresh.styleProperty().unbind();
        }
        
        refresh.selectedProperty().bindBidirectional(newObsDir.autoRefreshProperty());
        refresh.styleProperty().bind(Bindings.createStringBinding(() -> newObsDir.isGuiRefreshed() ? "-fx-background-color: green;" : "-fx-background-color: red;", newObsDir.isGuiRefreshedProperty()));

        
        doWhileIgnoringModelUpdates(() -> bindToList(newObsDir.childrenProperty()));
        
        path.setText(newObsDir.getDir().path());
        
        Platform.runLater(() -> list.requestFocus());
    }
    
    /**
     * Ignore events from model.
     */
    private void doWhileIgnoringModelUpdates(Runnable r) {
    	lockGettingBrowserFocus.set(true);
    	lockGettingBrowserSelection.set(true);
    	r.run();
    	lockGettingBrowserFocus.set(false);
    	lockGettingBrowserSelection.set(false);
    }

	private void bindToList(ObservableList<ObservableFileInfo> newModelList) {
        modelList = newModelList;
        
        if (sortedList != null) {
        	sortedList.comparatorProperty().unbind();
        }

		SortedList<ObservableFileInfo> basicSortedItems = new SortedList<>(modelList, this::basicDefaultSort);
        sortedList = new SortedList<>(basicSortedItems);

        list.setItems(sortedList);
        sortedList.comparatorProperty().bind(list.comparatorProperty());
	}
	
    private int basicDefaultSort(ObservableFileInfo a, ObservableFileInfo b) {
		int byType = TypeSorter.compareByType(a.isDirectory(), b.isDirectory());
		if (byType != 0) {
			return byType;
		}

		return NameSorter.compareByName(a.name(), b.name());
    }
}
