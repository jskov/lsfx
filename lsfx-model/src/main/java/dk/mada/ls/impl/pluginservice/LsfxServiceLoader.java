package dk.mada.ls.impl.pluginservice;

import java.util.ServiceLoader;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.mada.ls.api.plugin.LsfxPluginHook;
import dk.mada.ls.api.task.TaskRegistry;

@Singleton
public class LsfxServiceLoader {
    private static final Logger logger = LoggerFactory.getLogger(LsfxServiceLoader.class);
    private static ServiceLoader<LsfxPluginHook> pluginLoader = ServiceLoader.load(LsfxPluginHook.class);

    @Inject private TaskRegistry taskRegistry;
    
    public void loadPlugins() {
        logger.debug("Loading plugins");
        for (LsfxPluginHook hook : pluginLoader) {
            logger.debug("Load plugin {}", hook.getClass().getCanonicalName());
            hook.registerPlugins(taskRegistry);
        }
        logger.debug("Loading plugins completed");
    }
}
