package dk.mada.ls.impl.dir;

import java.io.IOException;
import java.net.URI;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.mada.ls.api.dir.Dir;
import dk.mada.ls.api.dir.Host;
import dk.mada.ls.api.dir.ImmutableDir;
import dk.mada.ls.api.dircache.model.FileInfo;
import dk.mada.ls.impl.dircache.DirCacheException;

/**
 * Provides utility operations on or for Dir instances.
 * 
 * FIXME: Should be implementing an interface for injection? How Java9 service?
 * 
 */
public class Dirs {
    private static final Logger logger = LoggerFactory.getLogger(Dirs.class);
    private static final String DEFAULT_FILESYSTEM_URI = "file:///";

    public static Dir from(String hostname, String path) {
    	var host = Hosts.fromHostname(hostname);
    	return createDir(host, DEFAULT_FILESYSTEM_URI, path);
    }
    
    public static Dir local(Path path) {
        return createDir(Hosts.localhost(), makeFilesystemUri(path), toAbsRealPathStr(path));
    }

    public static Dir local(String path) {
        return createDir(Hosts.localhost(), DEFAULT_FILESYSTEM_URI, toAbsRealPathStr(Paths.get(path)));
    }
    
    public static Dir resolve(Dir dir, FileInfo fileInfo) {
        String relPath = fileInfo.name();
        String oldPath = dir.path().replaceFirst("/$", "");
        
        String newPath;
        if (fileInfo.isSymbolicLink()) {
        	String targetPath = fileInfo.linkTargetPath();
        	if (targetPath.startsWith("/")) {
        		newPath = targetPath;
        	} else {
        		newPath = oldPath + "/" + targetPath;
        	}
        } else {
        	newPath = oldPath + "/" + relPath;
        }
        
        return createDir(dir.host(), dir.filesystemUri(), newPath);
    }

    public static Dir parent(Dir dir) {
    	if (dir.isRootDir()) {
    		return dir;
    	}
        String oldPath = dir.path();
        String parentPath = oldPath.replaceFirst("[^/]+/$", "");
        logger.debug("Parent path of {} is {}", oldPath, parentPath);
        return createDir(dir.host(), dir.filesystemUri(), parentPath);
    }

    public static Path toLocalPath(Dir dir) {
        if (!dir.host().isLocal()) {
            throw new IllegalStateException("Cannot make local Path from remote host " + dir.host());
        }
        return FileSystems.getFileSystem(URI.create(dir.filesystemUri())).getPath(dir.path());
    }
    
    public static String toAbsRealPathStr(Path localPath) {
        return toAbsRealPath(localPath).toString();
    }

    private static Path toAbsRealPath(Path path) {
        try {
            return path.toRealPath().toAbsolutePath();
        } catch (IOException e) {
            throw new DirCacheException("Failed to make path absolute " + path, e);
        }
    }
    
    private static String makeFilesystemUri(Path path) {
        String uri = path.getRoot().toUri().toString();
        if (uri.startsWith("jimfs")) {
            uri = uri.replaceFirst("/$", "");
        }
        return uri;
    }

    
    private static Dir createDir(Host host, String uri, String path) {
    	String pathWithTerminatingSlash = path.endsWith("/") ? path : (path+"/");
    	return ImmutableDir.builder()
    			.host(host)
    			.filesystemUri(uri)
    			.path(pathWithTerminatingSlash)
    			.build();
    }
}
