package dk.mada.ls.impl.dircache;

import org.immutables.value.Value.Immutable;

import dk.mada.ls.api.dircache.model.DirAttrs;
import dk.mada.ls.api.dircache.model.DirDelta;

@Immutable
public interface RemoteDirDelta {
    DirDelta dirDelta();
    DirAttrs dirAttrs();
}
