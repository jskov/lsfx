package dk.mada.ls.impl.dircache;

import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.EnumSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.Stream.Builder;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.mada.ls.api.dir.Dir;
import dk.mada.ls.api.dircache.model.FileInfo;
import dk.mada.ls.api.dircache.model.ImmutableFileInfo;

/**
 * Lists the current content of a given directory.
 * 
 * This should only be done once when starting to monitor a directory.
 * All subsequent changes should be updated via the DirChangeWatcher.
 */
public class LocalPathLister {
    private static final Logger logger = LoggerFactory.getLogger(LocalPathLister.class);
    @Inject private HostDirCacheUpdater dirCacheUpdater;

    public void list(LocalHostDirCache localHostDirCache, Path absPath, Dir dir) {
        dirCacheUpdater.updateWithList(localHostDirCache, dir, readPathAttributes(absPath), listFolder(absPath));
    }
    
    public Set<FileInfo> listFolder(Path folder) {
        logger.debug("Listing files in {}", folder);
        Builder<FileInfo> streamBuilder = Stream.builder();
        try {
            Files.walkFileTree(folder, EnumSet.noneOf(FileVisitOption.class), 1, new FileVisitor<Path>() {
                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    streamBuilder.accept(toFileInfo(file, attrs));
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                    throw new DirCacheException("Failed to list file " + file, exc);
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            throw new DirCacheException("Failed to list folder " + folder.toString(), e);
        }
        return streamBuilder.build().collect(Collectors.toSet());
    }

    public static BasicFileAttributes readPathAttributes(Path path) {
        try {
            return Files.readAttributes(path, BasicFileAttributes.class, LinkOption.NOFOLLOW_LINKS);
        } catch (IOException e) {
            throw new DirCacheException("Failed to read attributes on path " + path.toString(), e);
        }
    }

    private static Optional<BasicFileAttributes> readLinkAttributes(Path linkTargetPath) {
    	try {
    		return Optional.of(readPathAttributes(linkTargetPath));
    	} catch (DirCacheException e) {
    		logger.debug("Failed to read attributes on link target {}", linkTargetPath, e);
    		return Optional.empty();
    	}
    }

    private static Optional<Path> readLinkTarget(Path path) {
    	try {
    		Path target = Files.readSymbolicLink(path);
    		if (!target.isAbsolute()) {
    			target = path.getParent().resolve(target);
    		}
    		return Optional.of(target);
    	} catch (IOException e) {
    		logger.debug("Failed to read link target {}", path, e);
    		return Optional.empty();
    	}
    }

    /**
     * 
     * @param file
     * @return Optional FileInfo. Will be empty if accessing the file failed.
     */
    public static Optional<FileInfo> toFileInfo(Path file) {
        try {
            BasicFileAttributes basicAttributes = readPathAttributes(file);
			return Optional.of(toFileInfo(file, basicAttributes));
        } catch (DirCacheException e) {
            logger.debug("Bad access on {}", file, e);
            return Optional.empty();
        }
    }

    private static FileInfo toFileInfo(Path file, BasicFileAttributes attrs) {
    	Optional<Path> linkTarget = attrs.isSymbolicLink() ? readLinkTarget(file) : Optional.empty();
        Optional<BasicFileAttributes> linkTargetAttrs = linkTarget.flatMap(LocalPathLister::readLinkAttributes);
    	return toFileInfo(file, attrs, linkTarget, linkTargetAttrs);
    }
    
    private static FileInfo toFileInfo(Path file, BasicFileAttributes attrs, Optional<Path> linkTarget, Optional<BasicFileAttributes> linkTargetAttrs) {
        return ImmutableFileInfo.builder()
            .isDirectory(linkTargetAttrs.map(BasicFileAttributes::isDirectory).orElse(attrs.isDirectory()))
            .isFile(linkTargetAttrs.map(BasicFileAttributes::isRegularFile).orElse(attrs.isRegularFile()))
            .isSymbolicLink(attrs.isSymbolicLink())
            .linkTargetPath(linkTarget.map(Path::toString).orElse(null))
            .name(file.getFileName().toString())
            .size(attrs.size())
            .creationTime(attrs.creationTime().toMillis())
            .lastModifiedTime(attrs.lastModifiedTime().toMillis())
            .key(0)
            .build();
    }
}
