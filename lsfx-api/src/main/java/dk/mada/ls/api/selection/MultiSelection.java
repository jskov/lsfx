package dk.mada.ls.api.selection;

import java.util.List;

import dk.mada.ls.api.dircache.model.FileInfo;

/**
 * Represents multiple selected entries in the source browser (the file under the cursor
 * is not included).
 */
public interface MultiSelection extends Selection {
    List<FileInfo> getSelection();
}
