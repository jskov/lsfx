package dk.mada.ls.gui;

import dk.mada.ls.api.view.Tab;

public class DefaultTab implements Tab {
    public DefaultTab() {
    }
    
    @Override
    public String getName() {
        return "default tab";
    }
}
