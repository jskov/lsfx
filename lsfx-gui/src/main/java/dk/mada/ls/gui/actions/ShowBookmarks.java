package dk.mada.ls.gui.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.mada.ls.api.input.KeyBinding;
import dk.mada.ls.api.task.Context;
import dk.mada.ls.api.task.DefaultActionTask;
import dk.mada.ls.api.task.TaskFailedException;
import dk.mada.ls.plugins.LsfxDefaultPlugins;
import javafx.scene.input.KeyCode;

/**
 * Task to dive into a directory.
 */
public class ShowBookmarks extends DefaultActionTask {
	private static final Logger logger = LoggerFactory.getLogger(ShowBookmarks.class);
	
    public ShowBookmarks() {
        super(ShowBookmarks.class.getSimpleName(), LsfxDefaultPlugins.DEFAULT_PLUGINS_PROVIDER);
        withDefaultKeyBinding(KeyBinding.createWithControl(KeyCode.D));
    }

    @Override
    public void accept(Context context) throws TaskFailedException {
    	logger.info("Show bookmarks");
    	context.activeSourceBrowser.showBookmarks();
    }
}
