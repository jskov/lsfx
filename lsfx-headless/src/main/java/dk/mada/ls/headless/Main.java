package dk.mada.ls.headless;

import java.io.InputStream;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.logging.LogManager;

import javax.inject.Inject;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.mada.ls.cdi.CdiHelper;
import dk.mada.ls.impl.dir.Dirs;
import dk.mada.ls.impl.observables.DefaultObservableDir;
import dk.mada.ls.impl.observables.ObservableDirs;
import dk.mada.ls.impl.pluginservice.LsfxServiceLoader;
import javafx.embed.swing.JFXPanel;

public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);
    @Inject private LsfxServiceLoader serviceLoader;
    @Inject private ObservableDirs obsDirs;

    private void run() {
        loadLoggerConfiguration();
        setupJavaFX();
        
        serviceLoader.loadPlugins();

        DefaultObservableDir od = obsDirs.get(Dirs.local("/tmp"));

        logger.info("Got {}", od);
    }
    
    private static void setupJavaFX() {
        final CountDownLatch latch = new CountDownLatch(1);
        SwingUtilities.invokeLater(() -> {
            new JFXPanel(); // initializes JavaFX environment
            latch.countDown();
        });

        try {
            if (!latch.await(5L, TimeUnit.SECONDS)) {
                throw new IllegalStateException("JavaFx did no start");
            }
        } catch (InterruptedException e) {
            throw new IllegalStateException("JavaFx startup interrupted", e);
        }
    }
    
    private static void loadLoggerConfiguration() {
        final LogManager logManager = LogManager.getLogManager();
        try (final InputStream is = Main.class.getResourceAsStream("/logging.properties")) {
            logManager.readConfiguration(is);
        } catch (Exception e) {
            throw new IllegalStateException("Failed to load logger configuration", e);
        }
    }

    
    public static void main(String[] args) {
        CdiHelper.getInstance().getBean(Main.class).run();
    }
}
