package dk.mada.ls.gui.dir;

import java.nio.file.attribute.FileTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import dk.mada.ls.api.dir.ObservableFileInfo;
import javafx.scene.control.TableCell;

public class DateCell<T extends ObservableFileInfo> extends TableCell<T, Long> {
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm:ss");
    
    
    @Override
    protected void updateItem(Long item, boolean empty) {
        super.updateItem(item, empty);
        
        if (empty || item == null) {
            setText(null);
        } else {
            ZonedDateTime zdt = FileTime.fromMillis(item).toInstant().atZone(ZoneOffset.systemDefault());
            
            setText(FORMATTER.format(zdt));
        }        
    }

}
