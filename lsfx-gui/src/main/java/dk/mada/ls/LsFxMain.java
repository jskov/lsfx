package dk.mada.ls;

import java.io.InputStream;
import java.util.logging.LogManager;

import dk.mada.ls.gui.LsFxApp;
import dk.mada.ls.gui.fxapp.MainAppBootstrap;
import javafx.application.Application;

/**
 * LsFx's main class. It bootstraps the Fx application and CDI framework.
 */
public class LsFxMain extends MainAppBootstrap {
    public LsFxMain() {
        super("LsFx", LsFxMain.class.getPackage().getName(), LsFxApp.class);
    }

    public static void main(String[] args) {
        loadLoggerConfiguration();
        Application.launch(args);
    }

    private static void loadLoggerConfiguration() {
        final LogManager logManager = LogManager.getLogManager();
        try (final InputStream is = LsFxMain.class.getResourceAsStream("/logging.properties")) {
            logManager.readConfiguration(is);
        } catch (Exception e) {
            throw new IllegalStateException("Failed to load logger configuration", e);
        }
    }
}
