package dk.mada.ls.impl.prefs;

import java.nio.file.Path;
import java.nio.file.Paths;

public class Preferences {
	private static final Path PREFS_DIR = Paths.get(System.getProperty("user.home")).resolve(".local/share/lsfx");
	
	public Path getPreferencesDir() {
		return PREFS_DIR;
	}
}
