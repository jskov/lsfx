package dk.mada.ls.api.dir;

import org.immutables.value.Value.Derived;
import org.immutables.value.Value.Immutable;
import org.immutables.value.Value.Lazy;

/**
 * Directory defined by host, a filesystem lookup URI, and a path.
 */
@Immutable
public interface Dir {
    Host host();
    /**
     * Filesystem lookup URI.
     * @return Filesystem lookup URI.
     */
    String filesystemUri();
    String path();
    
    @Derived
    default String name() {
        return path().replaceFirst("/$", "").replaceFirst(".*/", "");
    }

    @Lazy
    default boolean isRootDir() {
    	return "/".equals(path());
    }
}
