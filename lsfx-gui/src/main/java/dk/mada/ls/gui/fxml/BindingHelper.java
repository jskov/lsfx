package dk.mada.ls.gui.fxml;

import java.util.Arrays;
import java.util.List;

import dk.mada.ls.gui.validation.Validation;
import javafx.beans.binding.BooleanExpression;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.Property;
import javafx.beans.property.StringProperty;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.util.converter.NumberStringConverter;

public class BindingHelper {
	private BindingHelper() {
		// use statics
	}

	public static void bindCheckbox(CheckBox checkBox, BooleanProperty prop) {
		checkBox.selectedProperty().bindBidirectional(prop);
	}
	
	public static <T> void bindCombo(ComboBox<T> comboBox, List<T> data, Property<T> prop, BooleanExpression validProp) {
		comboBox.getItems().setAll(data);
		comboBox.getSelectionModel().select(prop.getValue());
		prop.bind(comboBox.getSelectionModel().selectedItemProperty());
		Validation.bindIdValidationHint(comboBox, validProp);
	}

	public static <T> void bindCombo(ComboBox<T> comboBox, T[] data, Property<T> prop, BooleanExpression validProp) {
		bindCombo(comboBox, Arrays.asList(data), prop, validProp);
	}

	public static void bindText(TextField textField, StringProperty prop, BooleanExpression validProp) {
		textField.textProperty().bindBidirectional(prop);
		Validation.bindIdValidationHint(textField, validProp);
	}
	
	public static void bindText(TextArea textArea, StringProperty prop, BooleanExpression validProp) {
		textArea.textProperty().bindBidirectional(prop);
		Validation.bindIdValidationHint(textArea, validProp);
	}

	public static void bindTextNumber(TextField textField, IntegerProperty prop, BooleanExpression validProp) {
		textField.textProperty().bindBidirectional(prop, new NumberStringConverter());
		Validation.bindIdValidationHint(textField, validProp);
	}

	public static void unbindCheckbox(CheckBox checkBox, BooleanProperty prop) {
		checkBox.selectedProperty().unbindBidirectional(prop);
	}
	
	public static <T> void unbindCombo(ComboBox<T> field, ObjectProperty<T> prop) {
		prop.unbind();
		Validation.unbindId(field);
	}
	
	public static void unbindText(TextField field, StringProperty prop) {
		field.textProperty().unbindBidirectional(prop);
		Validation.unbindId(field);
	}

	public static void unbindText(TextField field, IntegerProperty prop) {
		field.textProperty().unbindBidirectional(prop);
		Validation.unbindId(field);
	}

	public static void unbindText(TextArea field, StringProperty prop) {
		field.textProperty().unbindBidirectional(prop);
		Validation.unbindId(field);
	}
}
