package dk.mada.ls.impl.dircache;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;

import java.io.IOException;
import java.nio.file.ClosedWatchServiceException;
import java.nio.file.FileSystem;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.mada.ls.api.dircache.model.FileInfo;
import dk.mada.ls.impl.dir.Dirs;

/**
 * Watches file system for changes to registered directories.
 * 
 * When one or more directories are watched, a utility thread is started
 * to handle events from changes to the directories.
 * When all directories are unregistered, the thread is stopped.
 */
@Singleton
public class LocalPathChangeWatcher {
    private static final Logger logger = LoggerFactory.getLogger(LocalPathChangeWatcher.class);

    private LocalHostDirCache localHostDirCache;
    private Map<FileSystem, WatchService> watchers = new HashMap<>();
    private Map<WatchService, Thread> watcherThreads = new HashMap<>();
    private LocalPathToWatchKeyMap pathWatchKeyMap = new LocalPathToWatchKeyMap();
    
    LocalPathChangeWatcher(LocalHostDirCache localHostDirCache) {
        this.localHostDirCache = localHostDirCache;
    }
    
    public synchronized void register(Path absPath) {
        pathWatchKeyMap.addIfPathNotContained(absPath, this::createWatchKey);
        start();
    }
    
    public synchronized void unregister(Path absPath) {
        pathWatchKeyMap.removeIfPathContained(absPath, (path, key) -> key.cancel());
        if (pathWatchKeyMap.isEmpty()) {
            stop();
        }
    }

    private synchronized void start() {
        watchers.values().forEach(this::startWatcherThread);
    }
    
    private void startWatcherThread(WatchService watcher) {
        watcherThreads.computeIfAbsent(watcher, ws -> {
            Thread watcherThread = new Thread(() -> sentinel(watcher), this.getClass().getSimpleName());
            watcherThread.start();
            logger.debug("Started dir change watcher in thread {} for {}", watcherThread, watcher);
            return watcherThread;
        });
    }
    
    private synchronized void stop() {
        watchers.values().forEach(this::stopWatcherThread);
        pathWatchKeyMap.clear();
    }
    
    private void stopWatcherThread(WatchService watcher) {
        watcherThreads.computeIfPresent(watcher,  (ws, watcherThread) -> {
            logger.info("Interrupting dir change watcher thread {}", watcherThread);
            watcherThread.interrupt();
            
            closeWatcher(watcher);
            
            return null;
        });
    }

    private void closeWatcher(WatchService watcher) {
        try {
            watcher.close();
        } catch (IOException e) {
            throw new DirCacheException("Failed to stop watcher", e);
        }
    }
    
    private void sentinel(WatchService ws) {
        try {
            logger.debug("Started sentinel for {}", ws);
            while (true) {
                WatchKey key = ws.take();
                logger.debug("Got event for {}", key);
                pathWatchKeyMap.getPath(key).ifPresent(path -> handleEvent(path, key));

                // This may return false if the key has been invalidated.
                // Does not affect this sentinel, as it handles multiple keys.
                key.reset();
            }
        } catch (ClosedWatchServiceException | InterruptedException e) {
            logger.debug("Dir change watcher {} received stop signal", Thread.currentThread(), e);
        }
        logger.debug("Exiting sentinel for {}", ws);
    }

    private void handleEvent(Path parentDir, WatchKey key) {
        Set<Path> added = new HashSet<>();
        Set<Path> changed = new HashSet<>();
        Set<String> removedNames = new HashSet<>();
        
        for (WatchEvent<?> event : key.pollEvents()) {
            WatchEvent.Kind<?> kind = event.kind();
            
            Path file = parentDir.resolve((Path) event.context());
            logger.debug("Change {} in {} : {}", kind, parentDir, file.getFileName());

            if (kind == ENTRY_DELETE) {
                removedNames.add(file.getFileName().toString());
                added.remove(file);
                changed.remove(file);
            } else if (kind == ENTRY_CREATE) {
                added.add(file);
                removedNames.remove(file.toString());
            } else if (kind == ENTRY_MODIFY) {
                changed.add(file);
            }
        }
        changed.removeAll(added);    // new files always (only) reported as added, even if changed
        
        logger.debug("Added {} Changed {} Removed {}", added, changed, removedNames);
        
        BasicFileAttributes dirAttrs = LocalPathLister.readPathAttributes(parentDir);
        
        localHostDirCache.updateWithFileDelta(Dirs.local(parentDir), dirAttrs, toFileInfos(added, removedNames), toFileInfos(changed, removedNames), removedNames);
    }
    
    private Set<FileInfo> toFileInfos(Set<Path> files, Set<String> removedNames) {
        Set<FileInfo> res = new HashSet<>();
        for (Path p : files) {
            Optional<FileInfo> fi = LocalPathLister.toFileInfo(p);
            if (fi.isPresent()) {
                res.add(fi.get());
            } else {
                removedNames.add(p.toString());
            }
        }
        return res;
    }
    
    private WatchKey createWatchKey(Path absPath) {
        try {
            WatchService watchService = getWatcher(absPath);
            return absPath.register(watchService, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
        } catch (IOException e) {
            throw new DirCacheException("Failed to register watcher for folder " + absPath, e);
        }
    }

    private WatchService getWatcher(Path path) {
        return watchers.computeIfAbsent(path.getFileSystem(), fs -> {
            try {
                logger.debug("Creating new watcher for filesystem {}", fs);
                return fs.newWatchService();
            } catch (IOException e) {
                throw new DirCacheException("Failed to create watch service for " + fs, e);
            }
        });
    }
}
