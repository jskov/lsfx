package dk.mada.ls.api.task;

import dk.mada.ls.api.selection.SingleSelection;

/**
 * Default single-selection tast.
 */
public abstract class DefaultSingleSelectionTask extends DefaultTask implements SingleSelectionTask {
    protected DefaultSingleSelectionTask(String name, String provider) {
        super(name, provider);
    }

    @Override
    public void accept(Context context, SingleSelection selection) throws TaskFailedException {
    }
    
    @Override
    public boolean alwaysWantsCursorSelection() {
        return false;
    }
}
