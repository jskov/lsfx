package dk.mada.ls.api.bookmark;

/**
 * Thrown on failes when loading/storing bookmark data.
 */
public class BookmarkStoreException extends Exception {
	public BookmarkStoreException(String message, Throwable cause) {
		super(message, cause);
	}
}
