package dk.mada.ls.gui.fxml;

import javafx.fxml.Initializable;
import javafx.scene.Parent;

public interface FxmlController extends Initializable {
	Parent getParentView();
}
