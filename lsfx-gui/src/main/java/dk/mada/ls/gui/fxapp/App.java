package dk.mada.ls.gui.fxapp;

import javafx.application.Application.Parameters;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public interface App {
	Parent getAppRootNode();
	
	default void preFxInit(Parameters parameters) {
	}
	default void preStageShow(Stage stage, Scene scene) {
	}
}
