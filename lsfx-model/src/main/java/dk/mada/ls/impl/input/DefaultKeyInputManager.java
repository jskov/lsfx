package dk.mada.ls.impl.input;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.mada.ls.api.input.KeyBinding;
import dk.mada.ls.api.selection.MultiSelection;
import dk.mada.ls.api.selection.Selection;
import dk.mada.ls.api.selection.SingleSelection;
import dk.mada.ls.api.task.ActionTask;
import dk.mada.ls.api.task.Context;
import dk.mada.ls.api.task.MultiSelectionTask;
import dk.mada.ls.api.task.SingleSelectionTask;
import dk.mada.ls.api.task.Task;
import dk.mada.ls.api.task.TaskFailedException;
import dk.mada.ls.api.view.Window;
import dk.mada.ls.impl.selection.DefaultMultiSelection;
import dk.mada.ls.impl.views.Windows;

@Singleton
public class DefaultKeyInputManager {
    private static final Logger logger = LoggerFactory.getLogger(DefaultKeyInputManager.class);

    private Map<KeyBinding, Task> bindings = new HashMap<>();
    @Inject private Windows windows;

    public void run(@Observes KeyBinding kb) {
        if (bindings.containsKey(kb)) {
            Task task = bindings.get(kb);
            logger.info("Key {} bound to {}", kb.getBinding(), task);
            activateTaskSafely(task);
        } else {
            logger.info("Nothing bound on {}", kb.getBinding());
        }
    }
    
    public void addDefaultBinding(KeyBinding defaultBinding, Task task) {
        if (bindings.containsKey(defaultBinding)) {
            throw new IllegalStateException("The keybinding " + defaultBinding.getBinding() + " is already bound to " + task);
        }
        bindings.put(defaultBinding, task);
    }
    
    private void activateTaskSafely(Task task) {
        try {
            activateTask(task);
        } catch (TaskFailedException e) {
            // TODO: add GUI feedback
            logger.warn("Failed to run task {}", task, e);
        }
    }
    
    private void activateTask(Task task) throws TaskFailedException {
        Optional<Context> optContext = windows.getActiveView().map(Window::getContext);
        if (!optContext.isPresent()) {
        	logger.info("No context for task activation");
        	return;
        }
        
        Context context = optContext.get();
        
        if (task instanceof ActionTask) {
            logger.info("Calling action {}", task.getName());
            ActionTask.class.cast(task).accept(context);
            return;
        }
        
        boolean preferCursorSelection = false;
        if (task instanceof SingleSelectionTask) {
            preferCursorSelection = SingleSelectionTask.class.cast(task).alwaysWantsCursorSelection();
        }
        
        Selection selection = context.activeSourceBrowser.getSelection(preferCursorSelection);
        if (task instanceof SingleSelectionTask && selection.isSingleSelection()) {
            logger.info("Calling action {} with selection {}", task.getName(), selection);
            SingleSelectionTask.class.cast(task).accept(context, SingleSelection.class.cast(selection));
            return;
        }
        
        if (task instanceof MultiSelectionTask) {
        	MultiSelection multiSelection = DefaultMultiSelection.fromSelection(selection);
        	MultiSelectionTask.class.cast(task).accept(context, multiSelection);
        	return;
        }
        
        throw new UnsupportedOperationException("TODO: handle multi-selection");
    }
}
