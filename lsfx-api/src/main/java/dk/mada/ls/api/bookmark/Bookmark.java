package dk.mada.ls.api.bookmark;

import org.immutables.value.Value.Immutable;

import dk.mada.ls.api.dir.Dir;

@Immutable
public interface Bookmark {
	String name();
	Dir location();
}
