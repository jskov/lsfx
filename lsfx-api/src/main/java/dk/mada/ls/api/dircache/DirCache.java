package dk.mada.ls.api.dircache;

import dk.mada.ls.api.dir.Dir;
import dk.mada.ls.api.dir.Host;
import dk.mada.ls.api.dircache.model.DirInfo;

/**
 * The DirCache allows a client to subscribe to changes in a directory.
 * 
 * These are posted as `DirInfo` events which the client should observe (via `@Observes`).
 * 
 * @see DirInfo
 */
public interface DirCache {
    /**
     * Clears cache.
     */
    void clear();

    /**
     * Clears cache for host.
     */
    void clear(Host host);

    /**
     * 
     */
    void subscribe(Dir dir);
    
    void unsubscribe(Dir dir);
    
    String toInfoString(); 

}
