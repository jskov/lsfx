package dk.mada.ls.api.view;

/**
 * Tab in a Window.
 * Can contain a browser, editor..
 */
public interface Tab {
    String getName();
}
