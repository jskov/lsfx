package dk.mada.ls.gui.dir;

import dk.mada.ls.api.dir.ObservableFileInfo;
import javafx.collections.ObservableList;
import javafx.scene.control.TableCell;

/**
 * Renderer for the Name column.
 * @author jskov
 *
 * @param <T>
 */
public class NameCell<T extends ObservableFileInfo> extends TableCell<T, String> {
    private static final String STYLE_CLASS_DIR_ROW = "dir-row";
	private static final String STYLE_CLASS_FILE_ROW = "file-row";
	private static final String STYLE_CLASS_LINK_ROW = "link-row";

	@Override
    protected void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
        
        if (empty || item == null) {
            setText(null);
        } else {
        	ObservableList<String> styleClasses = getStyleClass();
			styleClasses.removeAll(STYLE_CLASS_DIR_ROW, STYLE_CLASS_FILE_ROW);

        	T rowItem = getTableRow().getItem();
        	if (rowItem != null) {
				String baseTypeClass = rowItem.isDirectory() ? STYLE_CLASS_DIR_ROW : STYLE_CLASS_FILE_ROW;
	        	styleClasses.add(baseTypeClass);
	        	if (rowItem.isSymbolicLink()) {
	        		styleClasses.add(STYLE_CLASS_LINK_ROW);
	        	}
        	}
        	
            setText(item);
        }        
    }

}
