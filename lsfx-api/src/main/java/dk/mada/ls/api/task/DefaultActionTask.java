package dk.mada.ls.api.task;

/**
 * Default task that is not selection related.
 */
public abstract class DefaultActionTask extends DefaultTask implements ActionTask {
    protected DefaultActionTask(String name, String provider) {
        super(name, provider);
    }

    @Override
    public void accept(Context context) throws TaskFailedException {
    }
}
