package dk.mada.ls.plugins;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.mada.ls.api.plugin.LsfxPluginHook;
import dk.mada.ls.api.task.TaskRegistry;
import dk.mada.ls.plugins.dev.QuickExit;
import dk.mada.ls.plugins.editors.ClipboardPathTask;
import dk.mada.ls.plugins.editors.SimpleViewFileTask;
import dk.mada.ls.plugins.navigation.EnterPathTask;
import dk.mada.ls.plugins.navigation.ParentPathTask;
import dk.mada.ls.plugins.navigation.SwitchList;

public class LsfxDefaultPlugins implements LsfxPluginHook {
    private static final Logger logger = LoggerFactory.getLogger(LsfxDefaultPlugins.class);
    
    public static final String DEFAULT_PLUGINS_PROVIDER = "dk.mada.ls.plugins";

    @Override
    public void registerPlugins(TaskRegistry taskRegistry) {
        logger.info("Registering default plugins");
        Set.of(
        	ClipboardPathTask.class,
        	EnterPathTask.class,
        	ParentPathTask.class,
        	QuickExit.class,
        	SwitchList.class,
        	SimpleViewFileTask.class
    	).forEach(taskRegistry::addTask);
    }
}
