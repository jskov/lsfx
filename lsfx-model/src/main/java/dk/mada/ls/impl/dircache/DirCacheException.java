package dk.mada.ls.impl.dircache;

public class DirCacheException extends RuntimeException {
    public DirCacheException(String message, Throwable cause) {
        super(message, cause);
    }
}
