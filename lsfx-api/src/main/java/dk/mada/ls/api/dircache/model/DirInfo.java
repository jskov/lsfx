package dk.mada.ls.api.dircache.model;

import java.util.Set;

import org.immutables.value.Value.Immutable;

import dk.mada.ls.api.dir.Dir;

/**
 * Describes a directory by the state of the directory and its children.
 *
 * It does not contain any hierarchical information - it is accessed by its path through the `DirCache`.
 * 
 * children() represents all children. The deltas only the subset changed since last delta#.
 * The intention is to allow updating of only changed files in lists.
 * Also should be possible to reduce size of delta package from a remote backend.
 * 
 * @see DirCache
 */
@Immutable
public interface DirInfo {
    /**
     * Counter used to update DirInfo based on deltas.
     * @return count for last update via deltas
     */
    int deltaCounter();
    Dir dir();
    DirDelta dirDelta();
    DirAttrs dirAttrs();
    Set<FileInfo> children();
}
