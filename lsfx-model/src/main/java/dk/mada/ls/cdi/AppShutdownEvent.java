package dk.mada.ls.cdi;

/**
 * CDI event distributed when app is closing down.
 */
public class AppShutdownEvent {

}
