package dk.mada.ls.gui;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import dk.mada.ls.api.bookmark.BookmarkStoreException;
import dk.mada.ls.api.dir.Dir;
import dk.mada.ls.gui.actions.GuiActionRegistrator;
import dk.mada.ls.gui.fxapp.App;
import dk.mada.ls.gui.fxml.FxmlControllerBase;
import dk.mada.ls.gui.input.KeyboardHandler;
import dk.mada.ls.impl.bookmark.DefaultBookmarkStore;
import dk.mada.ls.impl.dir.Dirs;
import dk.mada.ls.impl.pluginservice.LsfxServiceLoader;
import dk.mada.ls.impl.thread.ThreadHelper;
import dk.mada.ls.impl.views.Windows;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.SplitPane;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class LsFxApp extends FxmlControllerBase implements App {
    @Inject private LsfxServiceLoader serviceLoader;
    @Inject private KeyboardHandler keyboardHandler;
    @Inject private Instance<DefaultWindow> windowInst;
    @Inject private Windows windows;
    @Inject private DefaultBookmarkStore bookmarkStore;
    @Inject private ThreadHelper threadHelper;
    @Inject private GuiActionRegistrator guiActions;
    
    @FXML private SplitPane split;
    @FXML private AnchorPane root;
    
	private DefaultWindow leftWindow;
	private DefaultWindow rightWindow;

    @Override
    public Parent getAppRootNode() {
        return root;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    	loadBookmarks();
    	
        Dir dir = Dirs.local("/tmp");

        leftWindow = windowInst.get();
        rightWindow = windowInst.get();
        
        leftWindow.setCompanion(Optional.of(rightWindow));
        rightWindow.setCompanion(Optional.of(leftWindow));
        
        leftWindow.showDir(dir);
        rightWindow.showDir(dir);

        split.getItems().addAll(leftWindow.getParentView(), rightWindow.getParentView());

        Platform.runLater(() -> windows.setActiveView(leftWindow));
    }

	private void loadBookmarks() {
		try {
			bookmarkStore.load();
		} catch (BookmarkStoreException e) {
			threadHelper.fxRun(() ->
				new Alert(AlertType.WARNING, "Failed to load bookmarks:\n" + e.getMessage()).showAndWait()
			);
		}
	}
    
    @Override
    public void preStageShow(Stage stage, Scene scene) {
        scene.addEventHandler(KeyEvent.KEY_PRESSED, keyboardHandler);

        guiActions.registerGuiTasks();
        serviceLoader.loadPlugins();
    }
}
