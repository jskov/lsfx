package dk.mada.ls.api.exception;

/**
 * Exception used when proper error handling has not been figured out.
 * This is a placeholder only!
 */
public class HandlingToBeDefinedException extends RuntimeException {
	private HandlingToBeDefinedException(String message) {
		super(message);
	}
	private HandlingToBeDefinedException(String message, Throwable cause) {
		super(message, cause);
	}
	
	@Deprecated
	public static HandlingToBeDefinedException todo(String handwaving, String message) {
		return new HandlingToBeDefinedException(message);
	}

	@Deprecated
	public static HandlingToBeDefinedException todo(String handwawing, String message, Throwable cause) {
		return new HandlingToBeDefinedException(message, cause);
	}
}

