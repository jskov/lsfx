package dk.mada.ls.api.dircache.model;

import org.immutables.value.Value.Immutable;

import dk.mada.ls.api.Nullable;
import dk.mada.ls.api.dircache.DirCache;

/**
 * Describes a file (child) in a directory.
 * 
 * There is no depth to the description of directory entries. They must
 * be looked up individually in the `DirCache`.
 *
 * A FileInfo is immutable, but the file it models may change (e.g. size changes).
 * dirId() can be used to compare two FileInfos - their place in the parent dir listing.
 * This is not the same as equals.
 * (Maybe equals method should be limited to this field? Not clear at present.)
 *  
 * 
 * @see DirCache
 */
@Immutable
public interface FileInfo {
    String name();
    long size();
    long lastModifiedTime();
    long creationTime();
    
    /**
     * Returns true if the file (or link target) is a regular file.
     * @return
     */
    boolean isFile();
    /**
     * Returns true if the file (or link target) is a directory.
     * @return
     */
    boolean isDirectory();
    /**
     * Returns true if the file is a link.
     * @return
     */
    boolean isSymbolicLink();
	@Nullable String linkTargetPath();
    
    /**
     * Use to distinguish files with the same name.
     * For most OS file systems this will always be 0.
     * In ZIP archives, it is used to index entries with the same name.
     *  
     * @return key for this file if relevant, or 0
     */
    int key();
    
    /**
     * DirId combines name and key so zip archive entries can be identified as different.
     * 
     * @return hash code for combination of key and name
     */
    default int dirId() {
        int h = 31+key();
        return h * 17 + name().hashCode();
    }
}
