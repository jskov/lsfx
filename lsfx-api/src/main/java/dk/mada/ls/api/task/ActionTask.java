package dk.mada.ls.api.task;

/**
 * Task that does not require input selection.
 */
public interface ActionTask extends Task {
	/**
	 * Accept event action with a given context.
	 * 
	 * Called from a worker thread.
	 * 
	 * @param context Context at the time of the task activation.
	 * @throws TaskFailedException if the task failed for some reason.
	 */
    void accept(Context context) throws TaskFailedException;
}
