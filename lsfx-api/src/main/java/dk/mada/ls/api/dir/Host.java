package dk.mada.ls.api.dir;

import org.immutables.value.Value.Immutable;

/**
 * Host identifier.
 */
@Immutable
public interface Host {
    String hostname();
    
    default boolean isLocal() {
        return "localhost".equals(hostname()) || "127.0.0.1".equals(hostname());
    }
}
