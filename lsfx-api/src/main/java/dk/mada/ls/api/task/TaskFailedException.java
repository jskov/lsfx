package dk.mada.ls.api.task;

public class TaskFailedException extends Exception {
    public TaskFailedException(String message) {
        super(message);
    }

    public TaskFailedException(String message, Throwable cause) {
        super(message, cause);
    }

}
