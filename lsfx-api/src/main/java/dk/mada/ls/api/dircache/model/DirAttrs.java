package dk.mada.ls.api.dircache.model;

import org.immutables.value.Value.Immutable;

import dk.mada.ls.api.dir.Dir;

/**
 * Describes directory attributes.
 */
@Immutable
public interface DirAttrs {
    Dir dir();
    long lastModifiedTime();
    long creationTime();
}
