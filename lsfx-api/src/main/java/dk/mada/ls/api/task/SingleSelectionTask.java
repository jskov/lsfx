package dk.mada.ls.api.task;

import dk.mada.ls.api.selection.SingleSelection;

public interface SingleSelectionTask extends Task {
    
    void accept(Context context, SingleSelection selection) throws TaskFailedException;
    
    /**
     * Allows task to get the cursor selection instead of a single
     * marked selection.
     * Return true to always get the FileInfo under the cursor location.
     * Return false to get the singled marked FileInfo selection, or the cursor
     * location path as a fallback.
     * 
     * @return cursor selection choice. 
     */
    boolean alwaysWantsCursorSelection();
}
