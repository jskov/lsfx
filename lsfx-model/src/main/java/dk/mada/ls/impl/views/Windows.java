package dk.mada.ls.impl.views;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.mada.ls.api.view.Window;

/**
 * Active windows in the current model.
 */
@Singleton
public class Windows {
	private static final Logger logger = LoggerFactory.getLogger(Windows.class);
    private Set<Window> windows = new HashSet<>();
    private Optional<Window> activeWindow = Optional.empty();
    
    public void add(Window window) {
        windows.add(window);
    }
    
    public Optional<Window> getActiveView() {
        return activeWindow;
    }
    
    public void setActiveView(Window window) {
    	logger.debug("Set focus on {}", window);
    	activeWindow.ifPresent(Window::losingFocus);
    	activeWindow = Optional.ofNullable(window);
    	activeWindow.ifPresent(Window::gettingFocus);
    }
}
