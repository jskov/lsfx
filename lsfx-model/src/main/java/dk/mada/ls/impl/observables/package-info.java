/**
 * Observable representation of directories.
 * 
 * The backend directory state - and that of the model - may change
 * while the user is interacting with the GUI.
 * 
 * How should this be handled to avoid confusion or errors?
 * 
 * It makes sense for the GUI to be refreshed if focus is lost/regained.
 * In this case, the user was doing other stuff, and would expect the
 * GUI to reflect current state when returning.
 * 
 * Or at the completion of a running operation, so the user can see
 * the result.
 * 
 * But while interacting with the GUI - e.g. selecting files to do
 * something with - refreshing GUI state will only cause confusion. 
 * 
 * So include a button that shows if the backing model changed - without
 * actually changing the shown directory listing content.
 * Clicking the button can force a refresh. Or maybe toggle that state
 * gets updated live, regardless of the above concerns.
 */

package dk.mada.ls.impl.observables;
