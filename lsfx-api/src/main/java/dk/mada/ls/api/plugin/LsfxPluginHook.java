package dk.mada.ls.api.plugin;

import dk.mada.ls.api.task.TaskRegistry;

public interface LsfxPluginHook {
    public void registerPlugins(TaskRegistry taskRegistry);

}
