package dk.mada.ls.gui.input;


import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.mada.ls.api.input.KeyBinding;
import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;

/**
 * Handles keyboard event, dispatching the appropriate task.
 * 
 * For now just registers tasks on their default binding.
 * Later should use task name/path to with user's saved preference bindings.
 *
 */
@Singleton
public class KeyboardHandler implements EventHandler<KeyEvent> {
    private static final Logger logger = LoggerFactory.getLogger(KeyboardHandler.class);

    private @Inject Event<KeyBinding> keyboardEvent; 

    @Override
    public void handle(KeyEvent event) {
        if (isIgnoredKeyCode(event)) {
            logger.debug("Key is ignored {}", event.getCode());
            return;
        }
        KeyBinding kb = KeyBinding.fromKeyEvent(event);
        logger.debug("Firing keyboard event {}", kb);
        keyboardEvent.fire(kb);
        event.consume();
    }

    private boolean isIgnoredKeyCode(KeyEvent event) {
        return event.getCode().isModifierKey();
    }
}
