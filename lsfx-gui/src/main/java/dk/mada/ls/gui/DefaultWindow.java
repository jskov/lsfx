package dk.mada.ls.gui;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.mada.ls.api.browser.Browser;
import dk.mada.ls.api.dir.Dir;
import dk.mada.ls.api.task.Context;
import dk.mada.ls.api.view.Tab;
import dk.mada.ls.api.view.Window;
import dk.mada.ls.gui.dir.DirList;
import dk.mada.ls.impl.browser.DefaultBrowser;
import dk.mada.ls.impl.views.Windows;
import javafx.application.Platform;
import javafx.scene.Parent;

public class DefaultWindow implements Window {
	private static final Logger logger = LoggerFactory.getLogger(DefaultWindow.class);
	
    @Inject private DefaultBrowser browser;
    @Inject private DirList dirList;
    @Inject private Windows views;

    private List<Tab> tabs = List.of(new DefaultTab());
    private Optional<Window> companion = Optional.empty();

    @PostConstruct
    private void register() {
    	views.add(this);
    	
    	dirList.setBrowser(browser);
    	dirList.setWidow(this);
    }
    
	public Parent getParentView() {
		return dirList.getParentView();
	}
    
    @Override
    public void showDir(Dir dir) {
    	browser.setDir(dir);
    }
    
	public void setCompanion(Optional<Window> companion) {
		this.companion = companion;
	}
	
	@Override
	public List<Tab> getTabs() {
		return Collections.unmodifiableList(tabs);
	}

	@Override
	public Optional<Window> getCompanion() {
		return companion;
	}

	@Override
	public Context getContext() {
		return new Context(this, browser, companion.map(Window::getBrowser));
	}

	@Override
	public Browser getBrowser() {
		return browser;
	}

	private AtomicBoolean focusViaModel = new AtomicBoolean();
	
	@Override
	public void gettingFocus() {
		Platform.runLater(() -> {
			logger.debug("Window getting focus {}", this);
			focusViaModel.set(true);
			dirList.requestFocus();
			focusViaModel.set(false);
		});
	}

	public void focusFromList(boolean gotFocus) {
		logger.debug("List got focus {} - via model {}", gotFocus, focusViaModel.get());
		if (gotFocus && !focusViaModel.get()) {
			views.setActiveView(this);
		}
	}
}
