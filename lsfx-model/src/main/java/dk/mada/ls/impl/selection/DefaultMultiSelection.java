package dk.mada.ls.impl.selection;

import java.util.List;
import java.util.Objects;

import dk.mada.ls.api.dircache.model.FileInfo;
import dk.mada.ls.api.selection.MultiSelection;
import dk.mada.ls.api.selection.Selection;
import dk.mada.ls.api.selection.SingleSelection;

public class DefaultMultiSelection implements MultiSelection {
    private List<FileInfo> selection;
    
    public DefaultMultiSelection(List<FileInfo> selection) {
        this.selection = selection;
    }

    public static MultiSelection fromSelection(Selection selection) {
    	Objects.requireNonNull(selection);
    	if (selection.isMultiSelection()) {
    		return MultiSelection.class.cast(selection);
    	}
    	List<FileInfo> single = List.of(SingleSelection.class.cast(selection).getSelection());
        return new DefaultMultiSelection(single);
    }

    public List<FileInfo> getSelection() {
        return selection;
    }

	@Override
	public boolean isSingleSelection() {
		return false;
	}

	@Override
	public boolean isMultiSelection() {
		return true;
	}
}
