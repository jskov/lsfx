package dk.mada.ls.api.dir;

import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyListProperty;

public interface ObservableDir {
    Dir getDir();
    ReadOnlyListProperty<ObservableFileInfo> childrenProperty();

    BooleanBinding isGuiRefreshedProperty();
    boolean isGuiRefreshed();
    
    BooleanProperty autoRefreshProperty();
}
