package dk.mada.ls.gui.fxml;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.scene.Parent;

public abstract class FxmlControllerBase implements FxmlController {
	private static final Logger logger = LoggerFactory.getLogger(FxmlControllerBase.class);
	private Parent parentView;
	
	@Inject private FxmlLoader loader;
	
	protected FxmlControllerBase() {
		// for cdi
	}
	
	protected FxmlControllerBase(FxmlLoader loader) {
		this.loader = loader;
	}
	
	@PostConstruct
	public void postConstruct() {
		loadFxml();
	}
	
	protected void loadFxml() {
		parentView = loadFxml(loader);
	}

	protected Parent loadFxml(FxmlLoader loader) {
		logger.debug("Loading FXML in {}:{}", this.getClass(), this.hashCode());
		return loader.load(this);
	}

	@Override
	public Parent getParentView() {
		return parentView;
	}
}
