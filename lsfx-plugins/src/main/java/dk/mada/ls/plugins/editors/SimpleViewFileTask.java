package dk.mada.ls.plugins.editors;

import java.nio.file.Path;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.mada.ls.api.dir.Dir;
import dk.mada.ls.api.dircache.model.FileInfo;
import dk.mada.ls.api.external.ExternalExecutor;
import dk.mada.ls.api.input.KeyBinding;
import dk.mada.ls.api.selection.SingleSelection;
import dk.mada.ls.api.task.Context;
import dk.mada.ls.api.task.DefaultSingleSelectionTask;
import dk.mada.ls.api.task.TaskFailedException;
import dk.mada.ls.impl.dir.Dirs;
import dk.mada.ls.plugins.LsfxDefaultPlugins;
import javafx.scene.input.KeyCode;

/**
 * Task to view a (local) file in a separate window/app.
 * 
 * For now hardwired to emacs. But should allow configuration
 * for any external editor invocation.
 */
public class SimpleViewFileTask extends DefaultSingleSelectionTask {
    private static final String SIMPLE_VIEWER = "emacs";
	private static final Logger logger = LoggerFactory.getLogger(SimpleViewFileTask.class);
    
    @Inject ExternalExecutor externalExecutor;
    
    public SimpleViewFileTask() {
        super(SimpleViewFileTask.class.getSimpleName(), LsfxDefaultPlugins.DEFAULT_PLUGINS_PROVIDER);
        withDefaultKeyBinding(KeyBinding.create(KeyCode.F3));
    }

    @Override
    public void accept(Context context, SingleSelection selection) throws TaskFailedException {
        logger.info("View file with {}", selection);
        
        Dir dir = context.activeSourceBrowser.getDir();
		if (!dir.host().isLocal()) {
			logger.info("Not a local dir, cannot do a simple view");
			return;
		}
        
        FileInfo selectedPath = selection.getSelection();
        if (selectedPath.isFile()) {
        	Path dirPath = Dirs.toLocalPath(dir);
        	externalExecutor.launchSimple(dirPath, SIMPLE_VIEWER, selectedPath.name());
        }
    }
}
