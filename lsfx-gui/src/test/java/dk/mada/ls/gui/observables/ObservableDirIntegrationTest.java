package dk.mada.ls.gui.observables;

import static java.nio.file.Files.createDirectories;
import static java.nio.file.Files.delete;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import org.jglue.cdiunit.AdditionalClasses;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;

import com.google.common.jimfs.Configuration;
import com.google.common.jimfs.Jimfs;
import com.google.common.jimfs.WatchServiceConfiguration;

import dk.mada.ls.CdiJfxTestBase;
import dk.mada.ls.api.dir.ObservableFileInfo;
import dk.mada.ls.impl.dir.Dirs;
import dk.mada.ls.impl.dircache.DefaultDirCache;
import dk.mada.ls.impl.observables.DefaultObservableDir;
import dk.mada.ls.impl.observables.ObservableDirs;

@EnabledIfEnvironmentVariable(named = "DISPLAY", matches = ".*")
@AdditionalClasses(DefaultDirCache.class)
public class ObservableDirIntegrationTest extends CdiJfxTestBase {
    private static final long JIMFS_SCAN_MS = 2;

    @Inject private ObservableDirs obsDirs;

    private Path rootTmpDir;
    private FileSystem filesystem;

    @BeforeEach
    public void prepareDisk() throws IOException {
        filesystem = Jimfs.newFileSystem("unit-test-fs", Configuration.unix()
                .toBuilder()
                .setWorkingDirectory("/")
                .setWatchServiceConfiguration(WatchServiceConfiguration.polling(JIMFS_SCAN_MS, TimeUnit.MILLISECONDS))
                .build());
        rootTmpDir = filesystem.getPath("/");
    }
    
    @AfterEach
    public void cleanupDisk() throws IOException {
        filesystem.close();
    }

    @Test
    public void whenDisabledChangesShouldNotHaveEffect() throws IOException, InterruptedException {
        Path dir = rootTmpDir.resolve("create-static");
        DefaultObservableDir obsDir = makeRefreshingObsDir(dir);
        obsDir.autoRefreshProperty().set(false);

        mkFile(dir.resolve("a"));

        sleepForJimfsEvents();

        assertThat(obsDir.childrenProperty())
            .isEmpty();
    }
    
    @Test
    public void pendingChangesShouldTakeEffectWhenUpdatesEnabled() throws IOException {
        Path dir = rootTmpDir.resolve("create-static-then-refresh");
        DefaultObservableDir obsDir = makeRefreshingObsDir(dir);
        obsDir.autoRefreshProperty().set(false);

        Path fileB = dir.resolve("b");
        Path fileC = dir.resolve("c");

        mkFile(dir.resolve("a"));
        mkFile(fileB);

        sleepForJimfsEvents();

        String testData = "test";
        mkFile(fileB, testData);
        mkFile(fileC, testData);

        sleepForJimfsEvents();
        
        assertThat(obsDir.childrenProperty())
            .isEmpty();

        obsDir.autoRefreshProperty().set(true);

        assertThat(mapToNames(obsDir))
            .containsExactlyInAnyOrder("a", "b", "c");

        // check that sizes have been updated
        List<String> resizedFiles = Arrays.asList("b", "c");
        assertThat(obsDir.childrenProperty().stream()
            .filter(ofi -> resizedFiles.contains(ofi.name()))
            .map(ObservableFileInfo::size))
            .hasSize(resizedFiles.size())
            .allMatch(l -> l == testData.length());
    }
    
    @Test
    public void deletionAndAppendsShouldBeUpdatedInDir() throws IOException {
        Path dir = rootTmpDir.resolve("create-with-refresh");
        DefaultObservableDir obsDir = makeRefreshingObsDir(dir);
                
        Stream.of("a", "b", "c")
            .map(dir::resolve)
            .forEach(this::mkFile);

        sleepForJimfsEvents();

        assertThat(mapToNames(obsDir))
            .containsExactlyInAnyOrder("a", "b", "c");
        
        // delete one file
        Path fileB = dir.resolve("b");
        delete(fileB);
        
        sleepForJimfsEvents();

        assertThat(mapToNames(obsDir))
            .containsExactlyInAnyOrder("a", "c");
        
        // recreate the file
        String testData = "test";
        mkFile(fileB, testData);
        mkFile(dir.resolve("c"), testData);

        sleepForJimfsEvents();

        assertThat(mapToNames(obsDir))
            .containsExactlyInAnyOrder("a", "b", "c");
        
        // check that sizes have been updated
        List<String> resizedFiles = Arrays.asList("b", "c");
        assertThat(obsDir.childrenProperty().stream()
            .filter(ofi -> resizedFiles.contains(ofi.name()))
            .map(ObservableFileInfo::size))
            .hasSize(resizedFiles.size())
            .allMatch(l -> l == testData.length());
    }

    private List<String> mapToNames(DefaultObservableDir obsDir) {
        return obsDir.childrenProperty().stream()
            .map(ObservableFileInfo::name)
            .collect(Collectors.toList());
    }
    
    private DefaultObservableDir makeRefreshingObsDir(Path testDir) throws IOException {
        createDirectories(testDir);
        DefaultObservableDir observableDir = obsDirs.get(Dirs.local(testDir));
        observableDir.autoRefreshProperty().set(true);
        return observableDir;
    }

    public void mkFile(Path path) {
        mkFile(path, null);
    }

    public void mkFile(Path path, String txt) {
        try {
            Files.deleteIfExists(path);
            Files.createFile(path);
            if (txt != null) {
                Files.write(path, txt.getBytes());
            }
        } catch (IOException e) {
            throw new IllegalStateException("Failed to create " + path, e);
        }
    }
    
    /**
     * Sleeps for JimFS events to fire.
     * Longer than the scan time to account for the update
     * actions happening on the FX thread.
     */
    private static void sleepForJimfsEvents() {
        try {
            Thread.sleep(20*JIMFS_SCAN_MS);
        } catch (InterruptedException e) {
            Thread.interrupted();
            throw new IllegalStateException("Timeout");
        }
    }

}
