package dk.mada.ls.fx;

import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/**
 * Change listener that only handles one change, then removes itself.
 * @param <T>
 */
public class OneShotChangeListener<T> implements ChangeListener<T> {
	private static final Logger logger = LoggerFactory.getLogger(OneShotChangeListener.class);
	private Consumer<T> onChange;

	public OneShotChangeListener(Consumer<T> onChange) {
		this.onChange = onChange;
	}
	
	@Override
	public void changed(ObservableValue<? extends T> obs, T ov, T nv) {
		logger.info("One shot triggered with {}", nv);
		onChange.accept(nv);
		obs.removeListener(this);
	}
}
