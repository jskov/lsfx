package dk.mada.ls.gui.dir;

/**
 * Sorting for file elements based on name and user preference.
 * 
 * User may select names ignoring case.
 */
public class NameSorter {
	private static final boolean PREFERENCE_SORT_IGNORE_CASE = true;

	public static int compareByName(String aName, String bName) {
		if (PREFERENCE_SORT_IGNORE_CASE) {
			return aName.compareToIgnoreCase(bName);
		} else {
			return aName.compareTo(bName);
		}
	}
}
