package dk.mada.ls.impl.dircache;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;

import dk.mada.ls.api.dir.Dir;
import dk.mada.ls.api.dircache.model.FileInfo;
import dk.mada.ls.impl.dir.Dirs;
import dk.mada.ls.impl.dir.Hosts;

/**
 * Local host dir cache.
 * 
 * Updated with access to file system.
 */
@Singleton
public class LocalHostDirCache extends HostDirCache {
    @Inject private HostDirCacheUpdater hostDirCacheUpdater;
    @Inject private LocalPathLister dirLister;
    
    private LocalPathChangeWatcher dirChangeWatcher;

    @PostConstruct
    private void bindToLocalhost() {
        withHost(Hosts.localhost());
        
        dirChangeWatcher = new LocalPathChangeWatcher(this);
    }
    
    public synchronized void list(Dir dir) {
        Path absPath = Dirs.toLocalPath(dir);
        if (!isSuitableForListing(absPath)) {
        	return;
        }
        dirChangeWatcher.register(absPath);
        dirLister.list(this, absPath, dir);
    }

	private boolean isSuitableForListing(Path absPath) {
        // TODO: How to handle if this is not a dir?
		return Files.isDirectory(absPath);
	}
    
    public synchronized void unsubscribe(Dir dir) {
        dirChangeWatcher.unregister(Dirs.toLocalPath(dir));
    }

    void updateWithFileDelta(Dir dir, BasicFileAttributes dirFileAttrs, Set<FileInfo> added, Set<FileInfo> changed, Set<String> removedNames) {
        hostDirCacheUpdater.updateWithFileDelta(this, dir, dirFileAttrs, added, changed, removedNames);
    }
}
