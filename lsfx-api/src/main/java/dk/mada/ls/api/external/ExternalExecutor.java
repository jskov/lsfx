package dk.mada.ls.api.external;

import java.nio.file.Path;

/**
 * Central hub for execution of external processes.
 * 
 * Intended for ease of debugging for the plugin developer.
 * 
 * Idea for implementation would be to allow seeing active
 * external processes in a window. See their output and kill them.
 */
public interface ExternalExecutor {
	/**
	 * Starts a new external process.
	 * 
	 * No polling/capture of output made, so if there is output
	 * it will probably cause the process to hang.
	 * Caller must deal with it.
	 * 
	 * @param dir Directory to execute from.
	 * @param args Command arguments.
	 * @return the created process.
	 */
	Process launchSimple(Path dir, String... args);
}
