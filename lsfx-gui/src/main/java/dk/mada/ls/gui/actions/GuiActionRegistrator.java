package dk.mada.ls.gui.actions;

import java.util.Set;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.mada.ls.api.task.TaskRegistry;

public class GuiActionRegistrator {
	private static final Logger logger = LoggerFactory.getLogger(GuiActionRegistrator.class);
	@Inject
	private TaskRegistry taskRegistry;

	public static final String DEFAULT_PLUGINS_PROVIDER = "dk.mada.ls.gui";

	public void registerGuiTasks() {

		logger.info("Registering default plugins");
		Set.of(ShowBookmarks.class).forEach(taskRegistry::addTask);
	}

}
