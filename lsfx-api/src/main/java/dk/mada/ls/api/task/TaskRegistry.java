package dk.mada.ls.api.task;

/**
 * Lets plugins register their tasks.
 */
public interface TaskRegistry {
    <T extends Task> void addTask(Class<T> taskType);
}
