package dk.mada.ls.plugins.navigation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.mada.ls.api.dir.Dir;
import dk.mada.ls.api.input.KeyBinding;
import dk.mada.ls.api.task.Context;
import dk.mada.ls.api.task.DefaultActionTask;
import dk.mada.ls.api.task.TaskFailedException;
import dk.mada.ls.impl.dir.Dirs;
import dk.mada.ls.plugins.LsfxDefaultPlugins;
import javafx.scene.input.KeyCode;

/**
 * Task to go up to parent directory.
 */
public class ParentPathTask extends DefaultActionTask {
    private static final Logger logger = LoggerFactory.getLogger(ParentPathTask.class);
    
    public ParentPathTask() {
        super(ParentPathTask.class.getSimpleName(), LsfxDefaultPlugins.DEFAULT_PLUGINS_PROVIDER);
        withDefaultKeyBinding(KeyBinding.create(KeyCode.BACK_SPACE));
    }

    @Override
    public void accept(Context context) throws TaskFailedException {
        Dir currentDir = context.activeSourceBrowser.getDir();
        Dir parentDir = Dirs.parent(currentDir);
        
        if (parentDir == currentDir) {
            logger.info("No parent of {}", currentDir);
        } else {
            logger.info("Go to parent of {} -> {}", currentDir, parentDir);
            context.activeSourceBrowser.setDir(parentDir, currentDir);
        }
    }
}
