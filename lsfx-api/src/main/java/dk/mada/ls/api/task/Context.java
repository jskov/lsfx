package dk.mada.ls.api.task;

import java.util.Optional;

import dk.mada.ls.api.browser.Browser;
import dk.mada.ls.api.view.Window;

/**
 * Context for a task activation.
 * 
 * Represents gui state at time of activation.
 */
public class Context {
	public final Window window;
    public final Browser activeSourceBrowser;
    public final Optional<Browser> activeTargetBrowser;
    
    public Context(Window window, Browser activeSourceBrowser, Optional<Browser> activeTargetBrowser) {
    	this.window = window;
        this.activeSourceBrowser = activeSourceBrowser;
        this.activeTargetBrowser = activeTargetBrowser;
    }
}
