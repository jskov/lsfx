package dk.mada.ls.impl.dircache;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import dk.mada.ls.api.dir.Dir;
import dk.mada.ls.api.dir.Host;
import dk.mada.ls.api.dircache.DirCache;

public class DefaultDirCache implements DirCache {
    @Inject private Instance<RemoteHostDirCache> remoteHostDirCacheInst; 
    @Inject private LocalHostDirCache localHostDirCache;
    
    private Map<Host, HostDirCache> remoteHost2cache = Collections.synchronizedMap(new HashMap<>());
    

    @Override
    public void subscribe(Dir dir) {
        if (dir.host().isLocal()) {
            localHostDirCache.list(dir);
        } else {
            subscribeOnRemote(dir);
        }
    }
    
    private void subscribeOnRemote(Dir dir) {
        HostDirCache hdc;
        synchronized(remoteHost2cache) {
             hdc = remoteHost2cache.computeIfAbsent(dir.host(), remoteHostDirCacheInst.get()::withHost);
        }
        throw new IllegalStateException("Cannot handle remote hosts yet - would use " + hdc);
    }

    @Override
    public void unsubscribe(Dir dir) {
        if (dir.host().isLocal()) {
            localHostDirCache.unsubscribe(dir);
        } else {
            throw new IllegalStateException("Cannot handle remote hosts yet");
        }
    }

    @Override
    public String toInfoString() {
        synchronized(remoteHost2cache) {
            return remoteHost2cache.values().stream()
                        .map(HostDirCache::toInfoString)
                        .collect(Collectors.joining(System.lineSeparator()));
        }
    }

    @Override
    public void clear() {
        remoteHost2cache.clear();
    }

    @Override
    public void clear(Host host) {
        HostDirCache hostDirCache = remoteHost2cache.get(host);
        if (hostDirCache != null) {
            hostDirCache.clear();
        }
    }
}
