package dk.mada.ls.impl.browser;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.mada.ls.api.browser.Browser;
import dk.mada.ls.api.dir.Dir;
import dk.mada.ls.api.dir.ObservableDir;
import dk.mada.ls.api.dircache.model.FileInfo;
import dk.mada.ls.api.selection.Selection;
import dk.mada.ls.fx.OneShotChangeListener;
import dk.mada.ls.impl.observables.DefaultObservableDir;
import dk.mada.ls.impl.observables.ObservableDirs;
import dk.mada.ls.impl.selection.DefaultEmptySelection;
import dk.mada.ls.impl.selection.DefaultMultiSelection;
import dk.mada.ls.impl.selection.DefaultSingleSelection;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.beans.property.ReadOnlyListProperty;
import javafx.beans.property.ReadOnlyListWrapper;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;

/**
 * Browser of paths.
 * 
 * Browser can navigate path trees and keeps track of focus and selection.
 * 
 * Intended to be bound to a list in the GUI.
 */
public class DefaultBrowser implements Browser {
    private static final Logger logger = LoggerFactory.getLogger(DefaultBrowser.class);
    @Inject private ObservableDirs obsDirs;

    private Dir dir;
    private DefaultObservableDir od;
    private ReadOnlyObjectWrapper<ObservableDir> currentDirProperty = new ReadOnlyObjectWrapper<>();
    private ReadOnlyListWrapper<Integer> selectedIndicesProperty = new ReadOnlyListWrapper<>(FXCollections.observableArrayList());
    private ReadOnlyIntegerWrapper cursorIndexProperty = new ReadOnlyIntegerWrapper(-1);
    private Runnable onShowBookmarks;
    
    @Override
    public Dir getDir() {
        return dir;
    }

    @Override
    public void setDir(Dir dir) {
        this.dir = dir;
        od = dir == null ? null : obsDirs.get(dir);
        od.justRevealedAcceptNextUpdate();
        currentDirProperty.set(od);
    }

    @Override
    public void setDir(Dir dir, Dir selectCursor) {
    	setDir(dir);
    	if (od.latestCounterProperty().get() > 0) {
    		setCursor(selectCursor);
    	} else {
    		od.activeCounterProperty().addListener(new OneShotChangeListener<Number>(n -> setCursor(selectCursor)));
    	}
    }
    
    @Override
    public ReadOnlyObjectProperty<ObservableDir> obsDirProperty() {
        return currentDirProperty.getReadOnlyProperty();
    }
    
    @Override
    public ReadOnlyIntegerProperty cursorIndexProperty() {
        return cursorIndexProperty.getReadOnlyProperty();
    }
    
    @Override
    public Optional<Integer> getCursorIndex() {
        int i = cursorIndexProperty.get();
        return i == -1 ? Optional.empty() : Optional.of(i);
    }
    
    @Override
    public Optional<FileInfo> getCursorItem() {
        return getCursorIndex().map(this::getFileInfo);
    }
    
    @Override
    public void setCursorIndex(int cursorIndex) {
        int newIndex = (cursorIndex >= getSize() || cursorIndex < 0) ? -1 : cursorIndex;
        logger.debug("Browser set cursor index at {}", newIndex);
        cursorIndexProperty.set(newIndex);
    }

    private void setCursor(Dir dir) {
        od.childrenProperty().stream()
            .filter(ofi -> ofi.getFileInfo().name().equals(dir.name()))
            .findFirst()
            .ifPresent(ofi -> setCursorIndex(od.childrenProperty().indexOf(ofi)));
    }
    
    private int getSize() {
        return od.childrenProperty().getSize();
    }
    
    private FileInfo getFileInfo(Integer i) {
        return od.childrenProperty().get(i).getFileInfo();
    }
    
    @Override
    public ReadOnlyListProperty<Integer> selectedIndicesProperty() {
        return selectedIndicesProperty.getReadOnlyProperty();
    }
    
    @Override
    public Selection getSelection(boolean preferCursorSelection) {
        Selection res;
        if (selectedIndicesProperty.isEmpty() || preferCursorSelection) {
            FileInfo single = getCursorItem().orElse(null);
            res = single == null ? new DefaultEmptySelection() : new DefaultSingleSelection(single);
        } else {
            List<FileInfo> selectedPaths = selectedIndicesProperty.stream()
                    .map(this::getFileInfo)
                    .collect(Collectors.toList());
            if (selectedPaths.size() == 1) {
                res = new DefaultSingleSelection(selectedPaths.get(0));
            } else {
                res = new DefaultMultiSelection(selectedPaths);
            }
        }
        logger.info("Returning selection {}", res);
        return res;
    }

    @Override
    public void selectIndices(List<Integer> selectedIndices) {
        logger.info("Browser selected {}", selectedIndices);
        selectedIndicesProperty.setAll(selectedIndices);
    }

	@Override
	public void showBookmarks() {
		if (onShowBookmarks != null) {
			onShowBookmarks.run();
		}
	}
	
	public void setOnShowBookmarks(Runnable onShowBookmarks) {
		this.onShowBookmarks = onShowBookmarks;
	}
}
