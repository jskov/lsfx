package dk.mada.ls.api.dir;

import dk.mada.ls.api.dircache.model.FileInfo;

public interface ObservableFileInfo {
    String name();
    int dirId();
    boolean isDirectory();
    boolean isSymbolicLink();
    FileInfo getFileInfo();
    long size();
    void update(FileInfo fi);
}
