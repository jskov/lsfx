package dk.mada.ls.gui.bookmark;

import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.mada.ls.api.bookmark.Bookmark;
import dk.mada.ls.api.bookmark.BookmarkStore;
import dk.mada.ls.api.browser.Browser;
import dk.mada.ls.api.view.Window;
import dk.mada.ls.impl.views.Windows;
import javafx.geometry.Side;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;

/**
 * Handles display of the bookmark menu.
 * 
 * TODO: How to handle directory bookmarks that are later changed to files?
 * Goto parent dir, select file? Would be handy for general goto-file stuff.
 */
@Singleton
public class BookmarksHandler {
    private static final Logger logger = LoggerFactory.getLogger(BookmarksHandler.class);

    @Inject private BookmarkStore bookmarkStore;
    @Inject private CreateBookmark createBookmark;
    @Inject private Windows windows;

	public void showForAnchor(Node anchor) {
        windows.getActiveView().map(Window::getContext)
        	.ifPresent(context -> show(context.activeSourceBrowser, anchor));
	}
	
	private void show(Browser browser, Node anchor) {
    	logger.info("Show bookmark items");
        var items = bookmarkStore.getBookmarks().stream()
        	.sorted((a, b) -> a.name().compareToIgnoreCase(b.name()))
        	.map(bm -> makeGotoDirMenuItem(bm, browser))
        	.collect(Collectors.toList());
        
        var addCurrentDir = new MenuItem("Add current dir");
        addCurrentDir.setOnAction(e -> {
        	if (browser != null) {
        		var dir = browser.getDir();
        		logger.info("Add dir {}", dir);
        		createBookmark.createBookmark(dir);
        	}
        });

        var contextMenu = new ContextMenu();
        var bookmarkItems = contextMenu.getItems();
		bookmarkItems.addAll(items);
        bookmarkItems.add(new SeparatorMenuItem());
        bookmarkItems.add(addCurrentDir);
        
        contextMenu.show(anchor, Side.BOTTOM, 0, 0);
    }
    
    private MenuItem makeGotoDirMenuItem(Bookmark bm, Browser browser) {
    	var mi = new MenuItem(bm.name());
    	mi.setOnAction(e -> {
    		logger.info("Goto bookmark {}", bm.location());
    		browser.setDir(bm.location());
    	});
    	return mi;
    }
}
