package dk.mada.ls.impl.dir;

import dk.mada.ls.api.dir.Host;
import dk.mada.ls.api.dir.ImmutableHost;

/**
 * Provides utility operations on or for Host instances.
 */
public class Hosts {
    private static final String LOCALHOST_NAME = "localhost";
	private static final Host LOCALHOST = ImmutableHost.builder().hostname(LOCALHOST_NAME).build();
    
    public static Host fromHostname(String hostname) {
    	if (LOCALHOST_NAME.equals(hostname)) {
    		return LOCALHOST;
    	}
    	return ImmutableHost.builder().hostname(hostname).build();
    }
    
    public static Host localhost() {
        return LOCALHOST;
    }
}
