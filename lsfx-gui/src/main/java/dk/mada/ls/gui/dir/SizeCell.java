package dk.mada.ls.gui.dir;

import dk.mada.ls.api.dir.ObservableFileInfo;
import javafx.scene.control.TableCell;

public class SizeCell<T extends ObservableFileInfo> extends TableCell<T, FileSize> {
    @Override
    protected void updateItem(FileSize item, boolean empty) {
        super.updateItem(item, empty);
        
        if (empty || item == null) {
            setText(null);
        } else {
        	setText(item.getSizeStr());
        }        
    }
}
