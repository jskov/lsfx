package dk.mada.ls.api.task;

import dk.mada.ls.api.selection.MultiSelection;

/**
 * Default multi-selection task.
 */
public abstract class DefaultMultiSelectionTask extends DefaultTask implements MultiSelectionTask {
    protected DefaultMultiSelectionTask(String name, String provider) {
        super(name, provider);
    }

    @Override
    public void accept(Context context, MultiSelection selection) throws TaskFailedException {
    }
}
