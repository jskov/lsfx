package dk.mada.ls.api.input;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class KeyBinding {
    private final String binding;
    
    private KeyBinding(KeyCode keyCode, boolean altDown, boolean controlDown, boolean metaDown, boolean shiftDown) {
        StringBuilder sb = new StringBuilder();
        if (altDown) {
            sb.append("ALT-");
        }
        if (controlDown) {
            sb.append("CTRL-");
        }
        if (metaDown) {
            sb.append("META-");
        }
        if (shiftDown) {
            sb.append("SHIFT-");
        }
        sb.append(keyCode.getName());
        this.binding = sb.toString();
    }
    
    public String getBinding() {
        return binding;
    }

    public static KeyBinding fromKeyEvent(KeyEvent event) {
        return createWithQualifiers(event.getCode(), event.isAltDown(), event.isControlDown(), event.isMetaDown(), event.isShiftDown());
    }

    public static KeyBinding create(KeyCode keyCode) {
        return createWithQualifiers(keyCode, false, false, false, false);
    }

    public static KeyBinding createWithAlt(KeyCode keyCode) {
        return createWithQualifiers(keyCode, true, false, false, false);
    }

    public static KeyBinding createWithAltShift(KeyCode keyCode) {
        return createWithQualifiers(keyCode, true, false, false, true);
    }

    public static KeyBinding createWithControl(KeyCode keyCode) {
        return createWithQualifiers(keyCode, false, true, false, false);
    }

    public static KeyBinding createWithShift(KeyCode keyCode) {
        return createWithQualifiers(keyCode, false, false, false, true);
    }

    public static KeyBinding createWithQualifiers(KeyCode keyCode, boolean altDown, boolean controlDown, boolean metaDown, boolean shiftDown) {
        return new KeyBinding(keyCode, altDown, controlDown, metaDown, shiftDown);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((binding == null) ? 0 : binding.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        KeyBinding other = (KeyBinding) obj;
        if (binding == null) {
            if (other.binding != null)
                return false;
        } else if (!binding.equals(other.binding))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "KeyBinding [binding=" + binding + "]";
    }
}
