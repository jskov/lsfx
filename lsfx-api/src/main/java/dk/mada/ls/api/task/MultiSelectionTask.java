package dk.mada.ls.api.task;

import dk.mada.ls.api.selection.MultiSelection;

public interface MultiSelectionTask extends Task {
    void accept(Context context, MultiSelection selection) throws TaskFailedException;
}
