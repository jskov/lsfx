package dk.mada.ls.gui.fxapp;


import java.net.URL;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;

import dk.mada.ls.cdi.AppShutdownEvent;
import dk.mada.ls.cdi.CdiHelper;
import dk.mada.ls.gui.fxml.FxmlLoader;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainAppBootstrap extends Application {
	private static final Logger logger = LoggerFactory.getLogger(MainAppBootstrap.class);
	private static final String ARG_CDI_LOGGER_DEBUG = "--debug-cdi";
	private static final String ARG_APP_LOGGER_DEBUG = "--debug-app";
	private static final String ARG_FXML_LOGGER_DEBUG = "--debug-fxml";
	private static final String FXML_LOGGER_ROOT = FxmlLoader.class.getPackage().getName();
	private static final String FXML_APP_LOGGER_ROOT = MainAppBootstrap.class.getPackage().getName();
	private static final String CDI_LOGGER_ROOT = "org.jboss.weld";
    private static final Map<Level, java.util.logging.Level> SLF4J_TO_JUL_LEVELS = Map.of(
            Level.ERROR, java.util.logging.Level.SEVERE,
            Level.WARN,  java.util.logging.Level.WARNING,
            Level.INFO,  java.util.logging.Level.INFO,
            Level.DEBUG, java.util.logging.Level.FINE,
            Level.TRACE, java.util.logging.Level.FINEST
            );
    
    private String version;
	private String appTitle;
	private Class<? extends App> appClass;
	private String appLoggerRootName;

	public MainAppBootstrap(String appTitle, String appLoggerRootName, Class<? extends App> appClass) {
		this.appTitle = appTitle;
		this.appLoggerRootName = appLoggerRootName;
		this.appClass = appClass;
	}
	
	@Override
	public void init() {
		String implementationVersion = this.getClass().getPackage().getImplementationVersion();
		version = (implementationVersion != null) ? implementationVersion : "<workspace>";

		if (parseDebugOptions()) {
			List<String> unnamed = getParameters().getUnnamed();

			
			boolean cdiDebug = unnamed.contains(ARG_CDI_LOGGER_DEBUG);
			boolean appDebug = unnamed.contains(ARG_APP_LOGGER_DEBUG);
			boolean fxmlDebug = unnamed.contains(ARG_FXML_LOGGER_DEBUG) || appDebug;
			
			setLevelOnLogger(appLoggerRootName, appDebug ? Level.DEBUG : Level.INFO);

			logger.debug("Unnamed args are {}", unnamed);
			logger.debug("Selecting log levels: cdi={}, app={}, fxml={}", cdiDebug, appDebug, fxmlDebug);
			
			setLevelOnLogger("org.apache", Level.WARN);
			
			setLevelOnLogger(CDI_LOGGER_ROOT, cdiDebug ? Level.DEBUG : Level.WARN);
			setLevelOnLogger(FXML_APP_LOGGER_ROOT, cdiDebug ? Level.DEBUG : Level.INFO);
			setLevelOnLogger(FXML_LOGGER_ROOT, fxmlDebug ? Level.DEBUG : Level.INFO);
		}
	}

	protected void setLevelOnLogger(String loggerName, Level level) {
        java.util.logging.Level julLevel = SLF4J_TO_JUL_LEVELS.get(level);
        logger.debug("Enabling {} ({}) output on {}", level, julLevel, loggerName);
        java.util.logging.Logger jul = java.util.logging.Logger.getLogger(loggerName);
		jul.setLevel(julLevel);
		logger.debug("Enabled {} ({}) output on {}", level, julLevel, loggerName);
	}

	protected boolean parseDebugOptions() {
		return true;
	}
	
	@Override
	public void start(Stage stage) {
		try {
			App app = CdiHelper.getInstance().getBean(appClass);
			if (app == null) {
				throw new IllegalStateException("Failed to get instance of " + appClass + " from CDI");
			}
			app.preFxInit(getParameters());

			loadWindowIcons(stage);
			Scene scene = makeScene(app.getAppRootNode());
			prepareStage(stage, scene);
			app.preStageShow(stage, scene);
			showStage(stage);
		} catch (Throwable e) { // NOSONAR
			logger.error("Application start failed", e);
			stop();
		}
	}

	protected Scene makeScene(Parent node) {
		Scene scene = new Scene(node);
		addCSSIfAvailable(scene);
		return scene;
	}
	
	protected void prepareStage(Stage stage, Scene scene) {
		stage.setTitle(appTitle + " - " + version);
		stage.setScene(scene);
	}

	protected void showStage(Stage stage) {
		stage.show();
	}

	@Override
	public void stop() {
		logger.info("Shutting down application");
		CdiHelper.getInstance().fireEvent(new AppShutdownEvent());
		Platform.exit();
		System.exit(0); // NOSONAR
	}
	
	private void loadWindowIcons(Stage stage) {
//		for (int size : new int[] {16, 24, 32, 48, 64}) {
//			stage.getIcons().add(new Image("/icons/icon-" + size + ".png", false));
//		}
	}

	private void addCSSIfAvailable(Scene scene) {
        URL uri = getClass().getResource(getCssName());
        if (uri == null) {
            return;
        }
        String uriToCss = uri.toExternalForm();
        scene.getStylesheets().add(uriToCss);
    }
	
	final String getCssName() {
		return getClass().getSimpleName() + ".css";
	}
}
