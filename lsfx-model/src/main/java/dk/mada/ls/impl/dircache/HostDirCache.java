package dk.mada.ls.impl.dircache;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.mada.ls.api.dir.Dir;
import dk.mada.ls.api.dir.Host;
import dk.mada.ls.api.dircache.model.DirInfo;

/**
 * Cache of directories on a given host.
 */
abstract class HostDirCache {
    private static final Logger logger = LoggerFactory.getLogger(HostDirCache.class);
    @Inject private Event<DirInfo> dirInfoEvent;
    
    private Host host;
    private Map<Dir, DirInfo> cache = Collections.synchronizedMap(new HashMap<>());
    
    protected HostDirCache withHost(Host host) {
        this.host = host;
        return this;
    }
    
    public void clear() {
        cache.clear();
    }

    /**
     * DirCacheUpdater hook into cache.
     * 
     * @param dir
     * @param creator
     * @param updator
     */
    void createOrUpdate(Dir dir, Supplier<DirInfo> creator, Function<DirInfo, DirInfo> updator) {
        synchronized(cache) {
            cache.compute(dir, (d, prevInfo) -> updateDirInfo(d, prevInfo, creator, updator));
        }
    }
    
    private DirInfo updateDirInfo(Dir dir, DirInfo prevInfo, Supplier<DirInfo> creator, Function<DirInfo, DirInfo> updator) {
        logger.debug("Update cache for {} with prev {}", dir.path(), prevInfo);
        DirInfo newDirInfo = (prevInfo == null) ? creator.get() : updator.apply(prevInfo);
        dirInfoEvent.fire(newDirInfo);
        return newDirInfo;
    }

    public String toInfoString() {
        StringBuilder sb = new StringBuilder("Dir cache for ").append(host).append(System.lineSeparator());
        synchronized(cache) {
            cache.keySet().stream()
                .sorted()
                .forEach(k -> sb.append(k).append(" : ").append(cache.get(k).toString()).append(System.lineSeparator()));
        }
        return sb.toString();
    }
}
