package dk.mada.ls.plugins.dev;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.mada.ls.api.input.KeyBinding;
import dk.mada.ls.api.task.Context;
import dk.mada.ls.api.task.DefaultActionTask;
import dk.mada.ls.api.task.TaskFailedException;
import dk.mada.ls.plugins.LsfxDefaultPlugins;
import javafx.scene.input.KeyCode;

/**
 * Task to dive into a directory.
 */
public class QuickExit extends DefaultActionTask {
	private static final Logger logger = LoggerFactory.getLogger(QuickExit.class);
	
    public QuickExit() {
        super(QuickExit.class.getSimpleName(), LsfxDefaultPlugins.DEFAULT_PLUGINS_PROVIDER);
        withDefaultKeyBinding(KeyBinding.create(KeyCode.ESCAPE));
    }

    @Override
    public void accept(Context context) throws TaskFailedException {
    	logger.warn("Quick exit!");
    	System.exit(0);
    }
}
