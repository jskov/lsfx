package dk.mada.ls.api.bookmark;

import java.util.List;

import dk.mada.ls.api.dir.Dir;

public interface BookmarkStore {
	void addBookmark(String name, Dir dir) throws BookmarkStoreException;
	List<Bookmark> getBookmarks();
}
